Hi, Welcome to the Quantum Gaussian Optics Toolkit (QGot) graphical user interface (gui)
repository.

## REQUIREMENTS

YOU NEED TO HAVE INSTALLED THE FOLLOWING LIBRARIES: 

  https://gitlab.com/OFThomas/olistd

  https://gitlab.com/dualityqp/qgot_public

# Installing Oli's standard library

  git clone https://gitlab.com/OFThomas/olistd.git && cd olistd
  
  mkdir build 

  cd build && cmake .. && cmake --build . -j4
  
  sudo cmake --install . 

# Installing QGot_public 

  git clone https://gitlab.com/dualityqp/qgot_public.git && cd qgot_public
  
  mkdir build 

  cd build && cmake .. && cmake --build . -j4
  
  sudo cmake --install . 


## To uninstall see the README.md in the respective git repositories

# To compile QGot_gui

  mkdir build 

  cd build 

(From the build directory)
 
 cmake .. && cmake --build . -j4

 Which will produce the executable `qgot` 


