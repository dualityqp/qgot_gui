## Readme for the start of the project in C++ using the Eigen3 lib for matrix objects

Begin experimental branch

// Tips
when plotting matrices in gnuplot use
set yrange [] reverse
to rotate the plot to match the matrix.

## Notes on Graphics_wrappers.xpp

All useful reusable graphics related functions are here.

Load texture from file takes the file location string

gnuplot_plot class takes data and plots image

show_me_image class used in imgui render loop, takes image data and can update image and
then load the texture.

## Notes on Gnuplot.hpp

Header only library that I have adapted, uses the POSIX pipe to pipe text to a gnuplot
process from cpp.

## ImGui docs

The documentation is lacking so I'm making notes on all the features I use / need
basically everything returns a bool.

use .. to put text or whatever on the same horizontal line  
ImGui::SameLine();

ImGui::Text("text");

# inputs

ImGui::Button("label", &btn_flag);
ImGui::RadioButton("label", &int_var, int btn_val);
ImGui::Checkbox("label", $check_flag);
ImGui::InputInt("label", &int_var);
ImGui::InputDouble("label", &double_var);
ImGui::SliderInt("label", &double_var, min, max);

if(ImGui::CollapsingHeader("Text"))
{
// sub menu stuff
}

# windows are made with

static bool close_flag = false;
if(close_flag)
{
ImGui::Begin("text", &close_flag);
// stuff
ImGui::End();
}
