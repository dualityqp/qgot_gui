/**
 * @author      : oli (oli@oli-HP)
 * @file        : gui_graphs
 * @created     : Tuesday Jan 25, 2022 15:48:26 GMT
 */

#ifndef GUI_GRAPHS_HPP
#define GUI_GRAPHS_HPP

#include "imnodes.h"
//#include "imnodes/example/save_load.cpp"
// #include "imnodes/example/node_editor.h"

#include "graphics_wrappers.hpp"
#include "qgot_public/graphs/graphs.hpp"

#include <math.h>
#include <memory>
#include <vector>
#include <sstream>

namespace gui
{
    // from imnodes example 
    namespace imnodes
    {
        struct Node
        {
            int   id;
            float value;

            Node() = default;

            Node(const int i, const float v = 0.0) : id(i), value(v) {}
        };

        struct Link
        {
            int id;
            int start_attr, end_attr;
            Link() = default;
            Link(int i, int st, int en) : id(i), start_attr(st), end_attr(en) {}
        };
        class SaveLoadEditor
        {
            public:
                SaveLoadEditor() : nodes_(), links_(), current_id_(0)
            {

                ImNodes::GetIO().LinkDetachWithModifierClick.Modifier = &ImGui::GetIO().KeyCtrl;
                ImNodes::PushAttributeFlag(ImNodesAttributeFlags_EnableLinkDetachWithDragClick);

            }

                // ~SaveLoadEditor()
                // {
                // ImNodes::PopAttributeFlag();
                // }

                void clean()
                {
                    ImNodes::PopAttributeFlag();
                }

                void set_nodes(const QGot::Graphs::Graph & graph)
                {
                    std::vector<int> vertices = graph.get_vertices();
                    int count = 0;
                    for(int vert : vertices)
                    {
                        nodes_.push_back(Node{vert+1});

                        float angle_offset = count * 2.0 * M_PI / (float)graph.size();
                        float radius = 400;
                        float x = radius * std::sin(angle_offset);
                        float y = radius * std::cos(angle_offset);

                        float offset = 20 + radius;
                        ImNodes::SetNodeEditorSpacePos(vert+1, {x + offset, y + offset});
                        count++;
                    }
                }

                void set_links(const QGot::Graphs::Graph & graph)
                {

                    std::vector<int> vertices = graph.get_vertices();
                    for(int vertex : vertices)
                    {
                        std::set<int> edges = graph.get_edges(vertex);

                        for(int edge : edges)
                        {
                            Link link;
                            int start_pin_idx = (vertex+1) << 24;
                            int end_pin_idx = (edge+1) << 8;
                            link.start_attr = start_pin_idx;
                            link.end_attr = end_pin_idx;
                            //if (ImNodes::IsLinkCreated(&link.start_attr, &link.end_attr))
                            //{
                            link.id = ++current_id_;
                            links_.push_back(link);
                            // }
                            // links_.push_back(Link{++current_id_,vertex+1, edge+1});
                        }
                    }
                }


                void show()
                {
                    ImGui::Begin("Save & load example");
                    ImGui::TextUnformatted("A -- add node");
                    ImGui::TextUnformatted(
                            "Close the executable and rerun it -- your nodes should be exactly "
                            "where you left them!");

                    ImNodes::BeginNodeEditor();

                    if (ImGui::IsWindowFocused(ImGuiFocusedFlags_RootAndChildWindows) &&
                            ImNodes::IsEditorHovered() && ImGui::IsKeyReleased('A'))
                    {
                        const int node_id = ++current_id_;
                        ImNodes::SetNodeScreenSpacePos(node_id, ImGui::GetMousePos());
                        nodes_.push_back(Node(node_id, 0.f));
                    }

                    for (Node& node : nodes_)
                    {
                        ImNodes::BeginNode(node.id);

                        // ImNodes::BeginNodeTitleBar();
                        // ImGui::TextUnformatted(std::to_string(node.id-1).c_str());
                        // ImNodes::EndNodeTitleBar();
                        ImNodes::BeginStaticAttribute(node.id << 16);
                        // ImGui::PushItemWidth(120.f);
                        // ImGui::DragFloat("value", &node.value, 0.01f);
                        ImGui::Text("%s", std::to_string(node.id-1).c_str());
                        ImGui::SameLine();
                        // ImGui::PopItemWidth();
                        ImNodes::EndStaticAttribute();


                        ImNodes::BeginInputAttribute(node.id << 8);
                        ImGui::TextUnformatted("in");
                        ImGui::SameLine();
                        ImNodes::EndInputAttribute();

                        ImNodes::BeginOutputAttribute(node.id << 24);
                        // const float text_width = ImGui::CalcTextSize("out").x;
                        // ImGui::Indent(120.f + ImGui::CalcTextSize("value").x - text_width);
                        ImGui::TextUnformatted("output");
                        ImNodes::EndOutputAttribute();

                        ImNodes::EndNode();
                    }

                    for (const Link& link : links_)
                    {
                        ImNodes::Link(link.id, link.start_attr, link.end_attr);
                    }

                    ImNodes::EndNodeEditor();

                    {
                        Link link;
                        if (ImNodes::IsLinkCreated(&link.start_attr, &link.end_attr))
                        {
                            link.id = ++current_id_;
                            links_.push_back(link);
                        }
                    }

                    {
                        int link_id;
                        if (ImNodes::IsLinkDestroyed(&link_id))
                        {
                            auto iter =
                                std::find_if(links_.begin(), links_.end(), [link_id](const Link& link) -> bool {
                                        return link.id == link_id;
                                        });
                            assert(iter != links_.end());
                            links_.erase(iter);
                        }
                    }

                    ImGui::End();
                }

                void save()
                {
                }

                void load()
                {
                }

                void get_positions()
                {
                    for(Node & node : nodes_)
                    {

                        std::string str = "Node " + std::to_string(node.id);
                        ImVec2 screen = ImNodes::GetNodeScreenSpacePos(node.id);
                        std::string screenpos = " Screenpos (" + std::to_string(screen.x) + "," + std::to_string(screen.y);
                        ImVec2 editor = ImNodes::GetNodeEditorSpacePos(node.id);
                        std::string editorpos = " Editorpos (" + std::to_string(editor.x) + "," + std::to_string(editor.y);
                        ImVec2 grid = ImNodes::GetNodeGridSpacePos(node.id);
                        std::string gridpos = " Gridpos (" + std::to_string(grid.x) + "," + std::to_string(grid.y);
                        ImGui::Text("%s", screenpos.c_str());
                        ImGui::SameLine();
                        ImGui::Text("%s", editorpos.c_str());
                        ImGui::SameLine();
                        ImGui::Text("%s", gridpos.c_str());
                    }
                }

            private:
                std::vector<Node> nodes_;
            public:
                std::vector<Link> links_;
                int               current_id_;
        };
    } // end of imnodes namespace

    class Graph
    {
        private:
            QGot::Graphs::Graph graph;
            int dim = 4;
            double density = 0.2;

        public:
            imnodes::SaveLoadEditor imnodes;
            Graph(int dim = 4, double density = 0.2) 
            {
                // graph.random(dim, density);
                // imnodes.set_nodes(graph);
                init(dim, density);
            }

            void init(int dim, double density)
            {
                graph.random(dim, density);
                imnodes = imnodes::SaveLoadEditor{};
                imnodes.set_nodes(graph);
                imnodes.set_links(graph);
            }

            void show()
            {

                static bool initialized = false;
                if (!initialized)
                {
                    initialized = true;
                    // example::NodeEditorInitialize();

                }

                //  ImGui::Begin("AGRAPSADJASD");
                ImGui::SliderInt("Dim", &dim, 2, 100);
                ImGui::SliderDouble("Density", &density, 0.01, 1.0);
                if(ImGui::Button("Generate Graph"))
                {
                    init(dim, density);
                }

                imnodes.show();
                imnodes.get_positions();

                for(auto link : imnodes.links_)
                {
                    std::stringstream stream;
                    stream << link.id << ", " << link.start_attr << ", " << link.end_attr;
                    std::string str = stream.str();
                    ImGui::Text("%s", str.c_str());
                }
                // example::NodeEditorShow();
                //    ImGui::End();
            }
    };

    // Draws a graph non-interactively 
    class Static_graph
    {
        private:

            class Node 
            {
                private:
                    float x;
                    float y;
                public:
                    // Node() = default;
                    Node(float x, float y) : x(x), y(y) {}

                    ImVec2 get_coords() const { return {x,y};}
            };

            class Edge
            {
                private:
                public:
                    Node & node1;
                    Node & node2;
                    // Edge() = default;
                    Edge(Node & node1, Node & node2) : node1(node1), node2(node2) {}

            };

            std::vector<Node> nodes;
            std::vector<Edge> edges;

            ImVec2 midpoint(const ImVec2 & p1, const ImVec2 & p2)
            {
                ImVec2 midp = {0.5f * (p1.x + p2.x), 0.5f * (p1.y + p2.y)};
                return midp;
            }

            void set_nodes(const QGot::Graphs::Graph & graph)
            {
                std::vector<int> vertices = graph.get_vertices();
                int count = 0;
                for([[maybe_unused]] int vert : vertices)
                {
                    float angle_offset = count * 2.0 * M_PI / (float)graph.size();
                    float radius = 100;
                    float x = radius * std::sin(angle_offset);
                    float y = radius * std::cos(angle_offset);

                    float offset = 200 + radius;
                    Node node{x + offset, y + offset};
                    nodes.push_back(node);
                    count++;
                }
            }

            void set_links(const QGot::Graphs::Graph & graph)
            {
                std::vector<int> vertices = graph.get_vertices();
                for(int vertex : vertices)
                {
                    std::set<int> edges_list = graph.get_edges(vertex);

                    for(int local_edge : edges_list)
                    {
                        Edge e{nodes[vertex], nodes[local_edge]};
                       edges.push_back(e);
                        // Link link;
                        // int start_pin_idx = (vertex+1) << 24;
                        // int end_pin_idx = (edge+1) << 8;
                        // link.start_attr = start_pin_idx;
                        // link.end_attr = end_pin_idx;
                        // //if (ImNodes::IsLinkCreated(&link.start_attr, &link.end_attr))
                        // //{
                        // link.id = ++current_id_;
                        // links_.push_back(link);
                        // // }
                        // // links_.push_back(Link{++current_id_,vertex+1, edge+1});
                    }
                }
            }

        public:

            Static_graph() = default;

            int dim = 4;
            double density = 0.2;

            void init(int dim, double density)
            {
                QGot::Graphs::Graph graph;
                graph.random(dim, density);
                // graph.add_vertex({0},{1,3});
                set_nodes(graph);
                set_links(graph);
            }
            /// See an example for drawing rings and waveguides in 
            // "graphics_code/graphics_tools.hpp" the Draw_Ring class
            void show()
            {
//                ImGui::Begin("Save & load example");
                // Not sure whether it is a good idea to have this here
                ImDrawList * draw_list = ImGui::GetWindowDrawList();

                ImU32 colour = IM_COL32(0,255,255,255);
                for(const Node & node : nodes)
                {
                    // do it 
                    ImVec2 centre = node.get_coords();
                    float radius = 20;
                    draw_list->AddCircleFilled(centre, radius, colour);

                }

                for(const Edge & edge : edges)
                {
                    // lookup the positions of the two nodes 
                    // in the edge and draw a line between them?
                    ImVec2 p1 = edge.node1.get_coords();
                    ImVec2 p4 = edge.node2.get_coords();
                    // find mid point 
                    ImVec2 midp = midpoint(p1,p4);

                    double angle = 0.5 * M_PI;
                    if((p1.x + p4.x) != 0.0)
                    {
                        double gradient = (p1.y + p4.y) / (p1.x + p4.x);
                        angle = atan(gradient);
                    }
                    angle = atan2(p1.y - p4.y, p1.x - p4.x);
                    double offset = 10;
                    ImVec2 p2 = midpoint(p1, midp);
                    p2.x += offset * cos(angle);
                    p2.y += offset * sin(angle);

                    ImVec2 p3 = midpoint(midp, p4);
                    p3.x -= offset * cos(angle);
                    p3.y -= offset * sin(angle);

                    float thick = 5;
                    std::cout << "Bezier points " << std::endl;
                    std::cout << p1.x << ", " << p1.y << std::endl;
                    std::cout << p2.x << ", " << p2.y << std::endl;
                    std::cout << p3.x << ", " << p3.y << std::endl;
                    std::cout << p4.x << ", " << p4.y << std::endl;
                //    draw_list->AddBezierCubic(p1, p2, p3, p4, colour, thick);
                // Cubic Bezier Curve (4 control points)
                
                    float x = p1.x;
                    float y = p1.y;
                    float sz = p4.x - p1.x;
                ImVec2 cp4[4] = { ImVec2(x, y), ImVec2(x + sz * 1.3f, y + sz * 0.3f), ImVec2(x + sz - sz * 1.3f, y + sz - sz * 0.3f), ImVec2(x + sz, y + sz) };
                draw_list->AddBezierCubic(cp4[0], cp4[1], cp4[2], cp4[3], colour, thick);
                }

                // ImGui::End();


                // draw_list -> PathArcTo(center1, radius, PI/2.0f, 3*PI/2.0, 100);
                // draw_list -> PathLineTo(ImVec2(center_x + length/2.0f, center_y - radius));
                // draw_list -> PathArcTo(center2, radius, -PI/2.0f, PI/2.0, 100);
                // bool closed{true};
                // draw_list -> PathStroke(col, closed, thickness);

            }
    };

} // end of gui namespace
#endif // end of include guard GUI_GRAPHS_HPP 

