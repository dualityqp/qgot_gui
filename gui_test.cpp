/** 
 * @file gui_main.cpp
 * @authors O Thomas
 * @date Feb 2020
 * @brief This file contains main for the GUI part of the code.
 * the structure is as follows: 
 * Sources are managed here,
 * Spectral filters are managed here,
 * Ring Resonator sources are managed here,
 * Optical Circuits are managed here.
 */


#include <unistd.h> // who knows...
#include "graphics_wrappers.hpp" // opengl and imgui wrappers for plots etc
#include "graphics_tools.hpp" // top level window managers, sources, filters, rings, circuits
#include "imguiutils.hpp" // window colours // theme 

#include <future> // std::async to stop blocking 

#include <iomanip> // cout and file spacing setw()
// testing only 
#include "docs.hpp" // documentation

#include "gui_graphs.hpp"
#include "imnodes.h"
#include "example/node_editor.h"

// [Win32] Our example includes a copy of glfw3.lib pre-compiled with
// VS2010 to maximize ease of testing and compatibility with old VS
// compilers.  To link with VS2010-era libraries, VS2015+ requires linking
// with legacy_stdio_definitions.lib, which we do using this pragma.  Your
// own project should not be affected, as you are likely to link with a
// newer binary of GLFW that is adequate for your version of Visual Studio.
#if defined(_MSC_VER) && (_MSC_VER >= 1900) && !defined(IMGUI_DISABLE_WIN32_FUNCTIONS)
#pragma comment(lib, "legacy_stdio_definitions")
#endif

static void glfw_error_callback(int error, const char* description)
{
    fprintf(stderr, "Glfw Error %d: %s\n", error, description);
}

/// Start of main, add functions here
int main(int, char**)
{
    // Setup window
    glfwSetErrorCallback(glfw_error_callback);

    if (!glfwInit())
        return 1;

    // Decide GL+GLSL versions
#if __APPLE__
    // GL 3.2 + GLSL 150
    const char* glsl_version = "#version 150";
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);  // 3.2+ only
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);            // Required on Mac
#else
    // GL 3.0 + GLSL 130
    const char* glsl_version = "#version 130";
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
    //glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);  // 3.2+ only
    //glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);            // 3.0+ only
#endif

    // Create window with graphics context
    GLFWwindow* window = glfwCreateWindow(960, 1024, "Quantum Gaussian optics toolkit", NULL, NULL);
    if (window == NULL)
        return 1;
    glfwMakeContextCurrent(window);
    glfwSwapInterval(1); // Enable vsync

    // Initialize OpenGL loader
#if defined(IMGUI_IMPL_OPENGL_LOADER_GL3W)
    bool err = gl3wInit() != 0;
#elif defined(IMGUI_IMPL_OPENGL_LOADER_GLEW)
    bool err = glewInit() != GLEW_OK;
#elif defined(IMGUI_IMPL_OPENGL_LOADER_GLAD)
    bool err = gladLoadGL() == 0;
#else
    bool err = false; // If you use IMGUI_IMPL_OPENGL_LOADER_CUSTOM, your loader is likely to requires some form of initialization.
#endif
    if (err)
    {
        fprintf(stderr, "Failed to initialize OpenGL loader!\n");
        return 1;
    }

    // blurry sampling? default is 0
    const float blur_fix = 0.0f;
    int screen_width, screen_height;
    glfwGetFramebufferSize(window, &screen_width, &screen_height);
    glViewport(blur_fix, blur_fix, screen_width + blur_fix, screen_height + blur_fix);

    // Setup Dear ImGui context
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO(); (void)io;
    io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;     // Enable Keyboard Controls
    //io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;      // Enable Gamepad Controls

    // Setup Dear ImGui style
    ImGui::StyleColorsDark();
    // ImGui::StyleColorsClassic();

    // change the colours here...
    // ImGui::SetupImGuiStyle(true, 1.0, 1);

    // Setup Platform/Renderer bindings
    ImGui_ImplGlfw_InitForOpenGL(window, true);
    ImGui_ImplOpenGL3_Init(glsl_version);

    // Imnodes
    ImNodes::CreateContext();
    // Load Fonts
    // - If no fonts are loaded, dear imgui will use the default font. You can also load multiple fonts and use ImGui::PushFont()/PopFont() to select them.
    // - AddFontFromFileTTF() will return the ImFont* so you can store it if you need to select the font among multiple.
    // - If the file cannot be loaded, the function will return NULL. Please handle those errors in your application (e.g. use an assertion, or display an error and quit).
    // - The fonts will be rasterized at a given size (w/ oversampling) and stored into a texture when calling ImFontAtlas::Build()/GetTexDataAsXXXX(), which ImGui_ImplXXXX_NewFrame below will call.
    // - Read 'docs/FONTS.txt' for more instructions and details.
    // - Remember that in C/C++ if you want to include a backslash \ in a string literal you need to write a double backslash \\ !
    std::string fontpath = "./libs/imgui/misc/fonts/";
    //io.Fonts->AddFontDefault();
    //  io.Fonts->AddFontFromFileTTF((fontpath + "Roboto-Medium.ttf").c_str(), 16.0f);
    //io.Fonts->AddFontFromFileTTF(fontpath + "Cousine-Regular.ttf", 15.0f);
    //io.Fonts->AddFontFromFileTTF(fontpath + "DroidSans.ttf", 16.0f);
    //io.Fonts->AddFontFromFileTTF(fontpath + "ProggyTiny.ttf", 10.0f);
    //ImFont* font = io.Fonts->AddFontFromFileTTF("c:\\Windows\\Fonts\\ArialUni.ttf", 18.0f, NULL, io.Fonts->GetGlyphRangesJapanese());
    //IM_ASSERT(font != NULL);

    // Our state
    bool show_another_window = false;
    ImVec4 clear_color = ImVec4(0.45f, 0.55f, 0.60f, 1.00f);
    // Main loop
    while (!glfwWindowShouldClose(window))
    {
        // Poll and handle events (inputs, window resize, etc.)
        // You can read the io.WantCaptureMouse, io.WantCaptureKeyboard flags to tell if dear imgui wants to use your inputs.
        // - When io.WantCaptureMouse is true, do not dispatch mouse input data to your main application.
        // - When io.WantCaptureKeyboard is true, do not dispatch keyboard input data to your main application.
        // Generally you may always pass all inputs to dear imgui, and hide them from your application based on those two flags.
        glfwPollEvents();

        // Start the Dear ImGui frame
        ImGui_ImplOpenGL3_NewFrame();
        ImGui_ImplGlfw_NewFrame();
        ImGui::NewFrame();

        // --------------------------------- PUT STUFF BELOW HERE ! ------------------

        // consts 
        static int power_counts = 1;
        static int jsa_size = 20; 
        static double squeezing_power = 0.1;

        static int num_loss_vals = 0;
        static int num_filter_vals = 1;

        static std::vector<int> filter_modes = {1,8,3,9};

        ImGui::Begin("Hello, world!");                         
        //ImGui::SetWindowSize(ImVec2(950,980));

        ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);

        static bool show_about_window = false;
        ImGui::SameLine(); 
        show_about_window |= ImGui::Button("About");

        ImGui::Separator();
        // ------------------------ Sources --------------------------------
        ImGui::BulletText("Source options");
        ImGui::Indent();
        ImGui::BulletText("Set Source properties in the source tab");
        ImGui::Unindent();

        // -------------------------- Filters --------------------------
        ImGui::BulletText("Filer options");
        ImGui::Indent();
        ImGui::BulletText("Set spectral filter properties in the Filter tab");
        ImGui::Unindent();

        // ------------------------ Circuit designer --------------------
        ImGui::BulletText("Circuit designer");
        ImGui::Indent();
        ImGui::BulletText(" Load a symplectic transformation (Only available on Linux)");
        ImGui::Unindent();

        // have to be global otherwise can't see each other?
        // one source 
        static std::vector<QGot::Source_graphics_menu> Sources = {QGot::Source_graphics_menu()};
        // Filters
        static std::vector<QGot::Filter_graphics_menu> Filters = {QGot::Filter_graphics_menu()};
        // one optical circuit 
        static std::vector<QGot::Circuit_graphics_menu> Circuits = {QGot::Circuit_graphics_menu()};

        // ------------------------------ Sources -------------------------
        if (ImGui::CollapsingHeader("Source options"))
        {
            ImGui::PushID("Sources");
            /// @TODO need to set jsa_spec size as global so same for all sources 
            static int source_counter = 1;
            ImGui::Text("Add or remove sources (%d)", source_counter); ImGui::SameLine();
            float spacing = ImGui::GetStyle().ItemInnerSpacing.x;
            ImGui::PushButtonRepeat(true);
            if (ImGui::ArrowButton("##left", ImGuiDir_Left) && source_counter)
            {
                source_counter--;
                while(source_counter < (int)Sources.size())
                {
                    // shrink
                    Sources.pop_back();
                }
            }
            ImGui::SameLine(0.0f, spacing);
            if (ImGui::ArrowButton("##right", ImGuiDir_Right))
            {
                source_counter++;
                while(source_counter > (int)Sources.size())
                {
                    // resize container 
                    Sources.push_back(QGot::Source_graphics_menu());
                }
            }
            ImGui::PopButtonRepeat();

            ImGui::SameLine(); ImGui::SliderInt("Spectral size", &jsa_size, 1, 250);        

            for(int i = 0; i < (int)Sources.size(); i++)
            {
                ImGui::PushID(i);
                Sources[i].show(jsa_size);
                ImGui::PopID();
            }
            ImGui::PopID();
        }
        // ------------------------- END OF SOURCES ------------------------

        // ------------------------------ Filters --------------------------
        if(ImGui::CollapsingHeader("Filter options"))
        {
            ImGui::PushID("Filters");
            for(int i = 0; i < (int)Filters.size(); i++)
            {
                ImGui::PushID(i);
                Filters[i].show(jsa_size);
                ImGui::PopID();
            }
            ImGui::PopID();
        }
        // ---------------------------- END OF FILTERS --------------------------


        /// --------------------------------- Circuit builders ---------------------
        /// state object which is passed to the circuit class, sets number of modes
        /// circuit class can change number of modes of the state object
        /// can load a covariance matrix into state from a file, the circuit class needs
        /// to know about this so it can update the circuit properly.
        /// histogram probabities are properties of the state and should be shown in main
        if(ImGui::CollapsingHeader("Circuit class builder"))
        {
            // int num_circuits = Circuits.size();
            ImGui::PushID("Circuits");

            static int circuit_counter = 1;
            ImGui::Text("Add or remove circuits (%d)", circuit_counter); ImGui::SameLine();
            float spacing = ImGui::GetStyle().ItemInnerSpacing.x;
            ImGui::PushButtonRepeat(true);
            if (ImGui::ArrowButton("##left", ImGuiDir_Left) && circuit_counter)
            {
                circuit_counter--;
                while(circuit_counter < (int)Circuits.size())
                {
                    // shrink
                    Circuits.pop_back();
                }
            }
            ImGui::SameLine(0.0f, spacing);
            if (ImGui::ArrowButton("##right", ImGuiDir_Right))
            {
                circuit_counter++;
                while(circuit_counter > (int)Circuits.size())
                {
                    // resize container 
                    Circuits.push_back(QGot::Circuit_graphics_menu());
                }
            }
            ImGui::PopButtonRepeat();

            for(int i = 0; i < (int)Circuits.size(); i++)
            {
                ImGui::PushID(i);
                ImGui::SameLine(); 

                static std::vector<QGot::Circuit_graphics_menu> Circuit_sweeps;
                bool static sweep_exists = false;
                static std::vector<double> powers;
                if(ImGui::Button("Power sweep"))
                {
                    olistd::Timer time_sweep;
                    // do awesome stuff 
                    // set sources powers to value 
                    // do simulation
                    // save Circuit object 
                    // open a new window which cycles through all of the circuit objects
                    // data plots 
                    // ???
                    // Profit!
                    double power_step = 0.2;
                    double st_power = 0.1;
                    int num_sweeps = 4;
                    Circuit_sweeps.resize(0);//num_sweeps);
                    powers.resize(0);
                    for(int j = 0; j < num_sweeps; j++)
                    {
                        powers.push_back(st_power + j * power_step);
                        std::cout << " power sweep, power = " << powers[j] << std::endl;

                        Circuits[i].set_up(jsa_size);
                        Circuits[i].run(jsa_size, Sources, Filters, true, powers[j]);

                        // ImGui::Columns(2, nullptr, false);

                        Circuits[i].show_circuit();

                        // ImGui::NextColumn();
                        Circuits[i].show_plots();
                        Circuits[i].show_covariance_plots();
                        // ImGui::Columns(1);


                        // save circuit 
                        Circuit_sweeps.push_back(Circuits[i]); 
                    }

                    sweep_exists = true;
                    time_sweep.stop();
                }
                ImGui::SameLine(); 
                static bool show_sweep = true;
                ImGui::Checkbox(" show sweeps ", &show_sweep); 

                // new window 
                if(sweep_exists && show_sweep)
                {
                    ImGui::Begin("SHOW SWEEPS", &show_sweep);   // Pass a pointer to our bool variable (the window will have a closing button that will clear the bool when clicked)
                    static bool show_circs = false; 
                    static bool show_plots = true;
                    ImGui::Checkbox("Show circuit", &show_circs); 
                    ImGui::SameLine();
                    ImGui::Checkbox("Show plots", &show_plots);

                    for(int k = 0; k < (int)Circuit_sweeps.size(); k++)
                    {
                        ImGui::PushID(k);
                        if(ImGui::CollapsingHeader("Power "))
                        {
                            ImGui::SameLine(); ImGui::Text("%f", powers[k]);
                            if(show_circs && show_plots) 
                            {
                                ImGui::Columns(2, nullptr, false);
                            }
                            else { ImGui::Columns(1, nullptr, false) ;}

                            if(show_circs)
                            {
                                Circuit_sweeps[k].show_circuit();
                                Circuit_sweeps[k].show_covariance_plots();
                                ImGui::NextColumn();
                            }
                            if(show_plots)
                            {
                                Circuit_sweeps[k].show_plots();
                            }
                            ImGui::Columns(1);
                        }
                        // show the power when the menu item is closed 
                        else { ImGui::SameLine(); ImGui::Text("%f", powers[k]); }
                        ImGui::PopID();
                    }
                    ImGui::End();
                } // end of sweeps pop_up window 

                // ----------------------------------------------------------------------
                // Main circuit menu stuff 

                Circuits[i].set_up(jsa_size);
                static bool show_detector_window = false;
                ImGui::SameLine();
                float reset_color = 0.0 ;
                ImGui::PushID("reset");

                ImGui::PushStyleColor(ImGuiCol_Button, (ImVec4)ImColor::HSV(reset_color, 0.6f, 0.6f));
                ImGui::PushStyleColor(ImGuiCol_ButtonHovered,(ImVec4)ImColor::HSV(reset_color, 0.7f, 0.7f));
                ImGui::PushStyleColor(ImGuiCol_ButtonActive, (ImVec4)ImColor::HSV(reset_color, 0.8f, 0.8f));

                // show_detector_window |= 
                if(ImGui::Button("Measurements", ImVec2(150,20)))
                    show_detector_window = !show_detector_window;

                ImGui::PopStyleColor(3);
                ImGui::PopID();

                ImGui::SameLine();
                // if the circuit is simulated .run returns true so we know to show the
                // results again 
                show_detector_window |= Circuits[i].run(jsa_size, Sources, Filters);
                // std::cout <<" before show circuit " << std::endl;

                // ImGui::Columns(2, nullptr, false);

                Circuits[i].show_circuit();

                // ImGui::NextColumn();

                // std::cout << "DB before show measurements, show_detector_window = " << show_detector_window << std::endl; 
                if(show_detector_window)
                {
                    ImGui::Begin("Measurements", &show_detector_window);
                    // ImGui::SetWindowFocus();
                    if(ImGui::Button("Close")) { show_detector_window = false;}


                    // std::cout << "DB before show_plots" << std::endl;
                    Circuits[i].show_plots();
                    // std::cout << "DB before show_cov_plots" << std::endl;
                    Circuits[i].show_covariance_plots();

                    ImGui::End();
                }
                // ImGui::Columns(1);

                // save circuit output
                Circuits[i].save_output();

                ImGui::PopID();
                ImGui::Separator();
                //    Circuits[i].show_cov();

            }
            ImGui::PopID();
        } // end of circuit stuff 
        // end of dropdown menus 
        // ------------------------- END OF CIRCUIT --------------------------------
        ImGui::Separator();
        if(ImGui::CollapsingHeader("Graphs"))
        {
            static gui::Graph graph;
            graph.show();

        static gui::Static_graph sgraph;

        if(ImGui::Button("Graph"))
        {
            sgraph = gui::Static_graph{};
            sgraph.init(4,0.2);
        }

        sgraph.show();
        }


        // -------------------------------- RING RESONATORS MODELLING ---------------
        // ring source generator
        ImGui::Separator();
        if(ImGui::CollapsingHeader("Ring circuits"))
        {
            static bool show_plots = true;
            static QGot::Ring_graphics_menu Ring;

            ImGui::Checkbox("Show plots", &show_plots);

            if(show_plots)
            {
                Ring.set_up();

                ImGui::Columns(2, nullptr, false);
                Ring.run();

                Ring.show();

                ImGui::NextColumn();
                Ring.show_transmission_powers();

                ImGui::Columns(1);

                Ring.save();

                Ring.show_symplectic();

            }
        }

        /*
        // -------------------------------- Don't worry about this 
        if(ImGui::CollapsingHeader("Derivatives"))
        {
        Derivative dx;
        static int val = 1;
        static int derivative = 0;
        ImGui::SliderInt("Evaluate 2x^2 + 3x + 1 at ", &val, 0, 100, "val = %d"); 
        ImGui::SliderInt("Derivative ", &derivative, 0, 100, "derivative = %d"); 

        dx.test_derivative(val, derivative);


        ImGui::TextWrapped("%s", dx.read_log().c_str());
        }
        */

        // ---------------------------------- Acknowledgements ---------------------
        if(show_about_window)
        {
            ImGui::Begin("About", &show_about_window);
            ImGui::Text("Yes"); ImGui::SameLine();
            if(ImGui::Button("Close")) { show_about_window = false;}
            // text here
            static std::string text;

            static std::string welcome_txt = "Welcome to a high performance, interactive Gaussian quantum optics toolkit written in C++";

            static std::string authors = "Oliver Thomas";

            static std::string contact = "oliver.thomas@bristol.ac.uk";

            static std::string acknowledge = "Will McCutcheon, Dara PS McCutcheon, John R Scott";

            text = welcome_txt + "\n Written by " + authors + "\n Contact " + contact;
            text += "\n With thanks to: " + acknowledge;

            std::string circuit_help = "Right click a gate to enter a value for the gate, squeezers set the squeezing parameter value, phase are in rads, beam splitter is the reflectivity"; 
            text += "Usage\n";
            text += circuit_help;
            ImGui::TextWrapped("%s",text.c_str());

            for(auto textvect : docs::ALL_DOCS)
            {

                for(auto text : textvect)
                {
                    ImGui::TextWrapped("%s", text.c_str());
                }
            }

            ImGui::End();
        }

        // ---------------------------------- OTHER STUFF ------------------------
        ImGui::Separator();
        if(ImGui::CollapsingHeader("Printing hom info"))
        {
            ImGui::PushItemWidth(200);
            const std::string hom_path = "../data/hom_data/";
            const std::string hom_source_path = "../data/hom_data/sources/";
            static std::string hom_filename_out = "homdip";
            if(ImGui::InputText("filename to write", &hom_filename_out))
            {
                std::cout << "name " << hom_filename_out << " end. Size = " << hom_filename_out.size() <<  std::endl;
            }
            ImGui::SameLine();
            // ImGui::NextColumn();
            if(ImGui::Button("Do it!"))
            {
                std::cout << "yeah!" << std::endl;
                std::string path = "";
                // write matrix 

            }

            static int s1 = 1;
            static int s2 = 1;
            ImGui::SliderInt("###1", &s1, 1, Sources.size(), "Source 1 = %d"); 
            ImGui::SameLine();
            ImGui::SliderInt("###2", &s2, 1, Sources.size(), "Source 2 = %d"); 

            static double power_step = 0.01;
            ImGui::SliderDouble("####12", &power_step, 0.000000000001, 2.0, "Power_step = %f");
            static int num_powers = 3;
            ImGui::SameLine();
            ImGui::SliderInt("####123", &num_powers, 1, 40, "Number of powers = %d");
            static int num_angles = 100;
            ImGui::SameLine();
            ImGui::SliderInt("####1234", &num_angles, 1, 600, "Number of angles = %d");

            static bool do_mzi = false;
            ImGui::SameLine();
            ImGui::Checkbox("MZI Dip", &do_mzi);
            static bool do_time_delay = true;
            ImGui::SameLine();
            ImGui::Checkbox("Time delay Dip", &do_time_delay);

            // make sources
            // hom_dip_power hom_result;
            // double power = 0.01;

            // int SETPRECISION = 6;
            // auto space = std::setw(2*SETPRECISION + 1);
            static std::string dip_time = "";
            if(ImGui::Button("Hom dip"))
            {
                QGot::Hom_dip homdip;
                // set the ouput directory 
                homdip.path = "../data/hom_data/";
                homdip.source_path = "../data/hom_data/sources/";
                homdip.n_spec = jsa_size;
                int dip_points = num_angles;
                // int dip_points = 261;// 160
                // dip_points = 501;
                //dip_points = 641;
                std::cout << "Enter source power: " << power_step << std::endl;
                // std::cin >> homdip.power;
                homdip.power = power_step;
                // std::cin >> filename;
                //  homdip.filter_width = homdip.n_spec/filter_width;

                homdip.set_s1(Sources[s1-1].make_jsa(jsa_size));
                homdip.set_s2(Sources[s2-1].make_jsa(jsa_size));

                // acts on modes 0&1 
                // homdip.set_filter();

                if(do_mzi)
                {
                    homdip.beamsplitter_scan(dip_points);
                    homdip.write(hom_filename_out);
                }
                if(do_time_delay)
                {
                    homdip.time_delay(dip_points);
                    homdip.write(hom_filename_out);
                }

            }
            ImGui::SameLine();
            ImGui::Text("Time taken = %s", dip_time.c_str());

            ImGui::PushItemWidth(-1);




        } // end of hom printing window 


        //if(ImGui::Button("Derivative test"))
        //{
        //    AD autodiff;
        //    autodiff.make_mat();
        //    autodiff.eval();
        //}


        bool hide_main_options = true; 
        if (ImGui::CollapsingHeader("Main options") && !hide_main_options) 
        {
            ImGui::Checkbox("Another Window", &show_another_window);
            ImGui::ColorEdit3("clear color", (float*)&clear_color); // Edit 3 floats representing a color
            if(show_another_window) 
            {
                ImGui::Begin("Another Window", &show_another_window);   // Pass a pointer to our bool variable (the window will have a closing button that will clear the bool when clicked)
            }
            ImGui::Text("Hello from another window!");
            if(ImGui::Button("Close Me")) { show_another_window = false; }

            // Start of main // 
            ImGui::Text("Hi, pick an option for the program to run \n 0 hom dips \n 1 nfold bucket test \n 2 g4 test \n 3 QFT fanout \n 4 Fusion gate \n 5 Nonlinear sign shift gate \n 6 Beamsplitter fanout \n 7 Spectral filtering \n ");

            if (ImGui::Button("Hom dip"))
            {
                ImGui::SameLine();
                ImGui::Text("run option 1");
                QGot::hom_routine(jsa_size, squeezing_power, power_counts, num_loss_vals, num_filter_vals);
            }

            if(ImGui::Button("Bucket detector coincidence probabilities")) 
            {
                QGot::do_nfold_coincidences(jsa_size, squeezing_power);
                ImGui::SameLine();
            }

            if(ImGui::Button("G4 test")) 
            {
                QGot::g4_test(jsa_size, squeezing_power);
                ImGui::SameLine();
            }

            if(ImGui::Button("QFT fanout")) 
            {
                QGot::qft_test(jsa_size, squeezing_power, 12);
                ImGui::SameLine();
            }

            if(ImGui::Button("Fusion gates")) 
            {
                QGot::fusion_gates();
                ImGui::SameLine();
            }

            if(ImGui::Button("KLM Nonlinear sign gate")) 
            {
                QGot::nonlinear_sign_shift_gate();
                ImGui::SameLine();
            }

            if(ImGui::Button("Fanout"))
            {
                QGot::beamsplitter_fanout();
                ImGui::SameLine();
            }

            if(ImGui::Button("Hom filtering"))
            {
                QGot::hom_routine_filters(jsa_size, squeezing_power, power_counts, num_loss_vals, filter_modes);
                ImGui::SameLine();
            }
            // 

            ImGui::InputInt("number of power values", &power_counts);
            ImGui::InputInt("number of loss values", &num_loss_vals);
            ImGui::InputInt("number of filter values", &num_filter_vals);
            // Functions for main window go here
            if(show_another_window)
            {
                ImGui::End();
            }
        }

        ImGui::End();

        // -------------------------------- Dont even think about changing this.
        // Rendering
        ImGui::Render();
        int display_w, display_h;
        glfwGetFramebufferSize(window, &display_w, &display_h);
        glViewport(blur_fix, blur_fix, display_w + blur_fix, display_h + blur_fix);
        glClearColor(clear_color.x, clear_color.y, clear_color.z, clear_color.w);
        glClear(GL_COLOR_BUFFER_BIT);
        ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

        glfwSwapBuffers(window);
    }
    example::NodeEditorShutdown();
ImNodes::DestroyContext();
    // Cleanup
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    ImGui::DestroyContext();

    glfwDestroyWindow(window);
    glfwTerminate();

    return 0;
}
