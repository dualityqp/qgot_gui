#include "olisstdlib.hpp"

int main()
{
    std::cout << "hi" << std::endl;

    std::vector<std::string> things;

    int n = 50; 
    int r = 6; 

    for(int i = 0; i < n; i++)
    {
        things.push_back(std::to_string(i));
    }

    olistd::print(things);

    // olistd::print(olistd::heap(things));

    olistd::Timer timer1;
    // olistd::print(olistd::nchooser(things, r));
    timer1.stop();


    long int terms = olistd::binomial_coeff(n,r);
    std::cout << "loop with terms = " <<  terms << std::endl;

    olistd::Timer timer2;
    for(long int i = 0; i < terms; i++)
    {
        olistd::print(olistd::static_nchooser(things, r));
    }
    timer2.stop();
    return 0;
}
