#pragma once
#ifndef graphics_wrappers_hpp
#define graphics_wrappers_hpp 

// #include "optical_circuits.hpp"
// #include "olistdlib.hpp"
#include <iterator>

// dear imgui: standalone example application for GLFW + OpenGL 3, using programmable pipeline
// If you are new to dear imgui, see examples/README.txt and documentation at the top of imgui.cpp.
// (GLFW is a cross-platform general purpose library for handling windows, inputs, OpenGL/Vulkan graphics context creation, etc.)

#include "imgui.h"
#include "imgui_impl_glfw.h"
#include "imgui_impl_opengl3.h"

#include "imgui_plot.h"
#include "imgui_stdlib.h"

#include "Eigen/Core"

#include <stdio.h>
#include <math.h>
#include <vector>

// debugg
#include <iostream>

/*
// About Desktop OpenGL function loaders:
//  Modern desktop OpenGL doesn't have a standard portable header file to load OpenGL function pointers.
//  Helper libraries are often used for this purpose! Here we are supporting a few common ones (gl3w, glew, glad).
//  You may use another loader/header of your choice (glext, glLoadGen, etc.), or chose to manually implement your own.
#if defined(IMGUI_IMPL_OPENGL_LOADER_GL3W)
#include <GL/gl3w.h>    // Initialize with gl3wInit()
#elif defined(IMGUI_IMPL_OPENGL_LOADER_GLEW)
#include <GL/glew.h>    // Initialize with glewInit()
#elif defined(IMGUI_IMPL_OPENGL_LOADER_GLAD)
#include <glad/glad.h>  // Initialize with gladLoadGL()
#else
#include IMGUI_IMPL_OPENGL_LOADER_CUSTOM
#endif
*/ 
// Include glfw3.h after our OpenGL definitions
#include <GLFW/glfw3.h>

namespace ImGui 
{

    IMGUI_API bool          SliderDouble(const char* label, double* v, double v_min, double v_max, const char* format = "%.3f", ImGuiSliderFlags flags = 0);   // olis double  


} // end of ImGui namespace 

// -------------------------------------- ImGui plotting for 1d data (Vectors)

class Plot
{
    private:
    public:
        std::vector<float> plot_data;
        ImU32 colors[3] = { ImColor(0, 255, 0), ImColor(255, 255, 0), ImColor(0, 255, 255) };
        uint32_t selection_start = 0, selection_length = 0;
        int num_plots;
        int buf_size;

        ImGui::PlotConfig conf;
        std::vector<Eigen::VectorXd> plot_dob;

        double min = 0.0;
        double max = 0.0;

        Plot() {}
        ~Plot() {}

        Plot(const std::vector<Eigen::VectorXd> & plots); 

        void highlight(double val);

        void xdata(float * xdata);

        void set_unit_scale() { min = -1; max = 1; }
        void show_inline(float x = 0, float y = 0);

        void show(float x = 0, float y = 0);
};

// Open gl plotting for 2d matrices to contour plots 
class opengl_plot
{
    private:
    public:
        int N;
        int Nrows;
        int Ncols;

        GLuint vbo[3];
        GLuint texture;

        bool interpolate_colours = true;
        bool use_hsv_interp = true;

        typedef struct {
            double r;       // a fraction between 0 and 1
            double g;       // a fraction between 0 and 1
            double b;       // a fraction between 0 and 1
        } rgb;

        typedef struct {
            double h;       // angle in degrees
            double s;       // a fraction between 0 and 1
            double v;       // a fraction between 0 and 1
        } hsv;

        std::vector<std::vector<unsigned int> > spectral_palette = { { 213, 62, 79 },
            { 244, 109, 67},
            { 253, 174, 97},
            { 254, 224, 139},
            { 230, 245, 152},
            { 171, 221, 164},
            { 102, 194, 165},
            { 50, 136, 189}};

        opengl_plot();


        hsv rgb2hsv(rgb in);
        rgb hsv2rgb(hsv in);

        GLuint rgb_interp_use_hsv(double scale, std::vector<unsigned int> rgb1, std::vector<unsigned int> rgb2);

        GLuint rgb_int(std::vector<unsigned int> rgb_vals);


        /// linearly interpolates between two rgb colour scale is 0 to 1 
        GLuint rgb_interp(double scale, std::vector<unsigned int> rgb1, std::vector<unsigned int> rgb2);

        int midpoint_search(double val, double bin_size, int max_k, double min_val =0.0);

        void update(const Eigen::MatrixXd & mat_in, double max_val_in = 0, int block_size = 0);

        void show(int x = 320, int y = 240, bool show_text = true);

        void show(bool * window_close);
};



bool LoadTextureFromFile(const char* filename, GLuint* out_texture, int* out_width, int* out_height);

#endif // graphics_wrappers_hpp
