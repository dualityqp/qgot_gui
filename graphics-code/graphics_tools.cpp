/*** @file graphics tools manager classes 
 * @author O Thomas 
 * @date Dec 2019
 */

#include "graphics_tools.hpp"

#include "qgot_public/math-code/matrix_io.hpp"

#include <future> // std::async
#include <deque> // replacement for std::vector<bool>

namespace QGot
{

    // Helper to display a little (?) mark which shows a tooltip when hovered.
    // In your own code you may want to display an actual icon if you are using a merged icon fonts (see docs/FONTS.txt)
    void HelpMarker(const char* desc)
    {
        ImGui::TextDisabled("(?)");
        if (ImGui::IsItemHovered())
        {
            ImGui::BeginTooltip();
            ImGui::PushTextWrapPos(ImGui::GetFontSize() * 35.0f);
            ImGui::TextUnformatted(desc);
            ImGui::PopTextWrapPos();
            ImGui::EndTooltip();
        }
    }

    // ------------------------------------------------ Source options --------------------
    Source_graphics_menu::Source_graphics_menu() 
    {
        update_jsa = false;
        reset();
    }

    Source_graphics_menu::~Source_graphics_menu() {}

    void Source_graphics_menu::reset()
    {
        func_choice = "Sinc";
        xoffset = 0;
        yoffset = 0;
        phasematching = 29.0;
        bandwidth = 0.1;
        update_jsa = true;
        jsa_size = 50;
        squeezing_power = 0.1;
        new_window = false;
        jsa = make_jsa(jsa_size); // Jsa(jsa_size, squeezing_power, func_choice, xoffset, yoffset);
    }

    void Source_graphics_menu::show(int spec_size)
    {
        if(jsa_size != spec_size) 
        {   
            jsa_size = spec_size;
            update_jsa = true;
        }
        ImGui::Separator();
        ImGui::Text("Source editor tools");
        // columns 
        ImGui::Columns(5);
        // ImGui::SetColumnWidth(0, 320.0f);
        // ImGui::SetColumnWidth(1, 290.0f);
        // ImGui::SetColumnWidth(2, 200.0f);
        // ImGui::SetColumnWidth(3, 240.0f);


        // static show_me_image jsa_image;
        // check if any of the params change and make a new jsa obj and plot it
        // to show it 
        if(update_jsa)
        {
            olistd::Timer time_jsa;
            update_jsa = false;
            changed_flag = true;
            jsa = make_jsa(jsa_size);
            if(jsa_size > 1)
                plot.update(jsa.matrix.cwiseAbs());
            time_taken = time_jsa.get_time();
        }
        ImGui::SameLine(); ImGui::Text("Took %s", time_taken.c_str());

        if(jsa_size > 1)
        {
            if(new_window) 
            { 
                plot.show(&new_window);
            }
            else 
            {
                plot.show();
            }
        }
        // source options 
        ImGui::NextColumn();
        // reset all vals to default 
        float reset_color = 0.0 ;
        ImGui::PushID("reset");

        ImGui::PushStyleColor(ImGuiCol_Button, (ImVec4)ImColor::HSV(reset_color, 0.6f, 0.6f));
        ImGui::PushStyleColor(ImGuiCol_ButtonHovered,(ImVec4)ImColor::HSV(reset_color, 0.7f, 0.7f));
        ImGui::PushStyleColor(ImGuiCol_ButtonActive, (ImVec4)ImColor::HSV(reset_color, 0.8f, 0.8f));

        if(ImGui::Button("Reset", ImVec2(100,20)))
        {
            reset();
        }
        ImGui::PopStyleColor(3);
        ImGui::PopID();

        ImGui::SameLine(); ImGui::Checkbox("new window", &new_window); 
        if(ImGui::Checkbox("Colour interpolation", &plot.interpolate_colours))
        { 
            update_jsa = true; 
        } 
        ImGui::SameLine();
        if(ImGui::Checkbox("HSV interp", &plot.use_hsv_interp))
        { 
            update_jsa = true; 
        }

        ImGui::SameLine(); 
        HelpMarker("CTRL+click to input value.");
        update_jsa |= ImGui::InputDouble("Power", &squeezing_power, 0.01f, 1.0f, "%.8f");
        ImGui::SameLine(); 
        HelpMarker("You can apply arithmetic operators +,*,/ on numerical values.\n  e.g. [ 100 ], input \'*2\', result becomes [ 200 ]\nUse +- to subtract.\n");


        // yes
        if(ImGui::ListBoxHeader("functions"))
        {
            for(int i = 0; i < (int)jsa_funcs_list.size(); i++)
            {
                if(ImGui::Selectable(jsa_funcs_list[i].name.c_str(), jsa_funcs_list[i].selected))
                {
                    // std::cout << "selected " << jsa_funcs_list[i].name << std::endl;
                    for(int j = 0; j < (int)jsa_funcs_list.size(); j++)
                    {
                        jsa_funcs_list[j].selected = false;
                    }
                    jsa_funcs_list[i].selected = true;
                    update_jsa = true;
                    func_choice = jsa_funcs_list[i].name;
                }
            }
            ImGui::ListBoxFooter();
        }

        // phase matching slider 
        update_jsa |= ImGui::SliderDouble("Phase matching", &phasematching, 0.0,60.0,"%.4f" ); 
        update_jsa |= ImGui::SliderDouble("Pump bandwidth", &bandwidth, 0.0,1.0,"%.4f" ); 

        // list for funtions 
        update_jsa |= ImGui::SliderDouble("Signal offset", &xoffset, -3,3,"%.4f" ); 
        ImGui::SameLine(); HelpMarker("CTRL+click to input value.");
        update_jsa |= ImGui::SliderDouble("Idler offset", &yoffset, -3,3,"%.4f");
        ImGui::SameLine(); HelpMarker("CTRL+click to input value.");

        // photon numbers plots 
        ImGui::NextColumn();
        ImGui::PushItemWidth(100);

        ImGui::SetNextItemOpen(true, ImGuiCond_FirstUseEver);
        if(ImGui::CollapsingHeader("Avg photon number"))
        {
            // stuff here
            //average photon num = sinh^2(power)
            static int max_points = 200;
            double x_spacing = 0.01;
            Eigen::VectorXd x_data(max_points); //, 0.0);
            Eigen::VectorXd y_data(max_points); //, 0.0);

            for(int i = 0; i < max_points; i++)
            {
                x_data(i) = x_spacing * i;
                y_data(i) = 1.0 * std::pow(sinh(x_data(i)),2);
                // per mode in the two mode squeezer is sinh^2
            }
            Plot avg_photon_num_plot({y_data});

            avg_photon_num_plot.highlight(squeezing_power);

            int index = squeezing_power / x_spacing;
            ImGui::SameLine();
            ImGui::Text("%.3f", y_data[index]);

            avg_photon_num_plot.show_inline(180);
        }

        ImGui::SetNextItemOpen(true, ImGuiCond_FirstUseEver);
        if(ImGui::CollapsingHeader("Bucket click prob"))
        {
            // stuff here
            static int max_points = 200;
            double x_spacing = 0.01;
            Eigen::VectorXd x_data(max_points); //, 0.0);
            Eigen::VectorXd y_data(max_points); //, 0.0);

            for(int i = 0; i < max_points; i++)
            {
                x_data(i) = x_spacing * i;
                y_data(i) = std::pow(tanh(x_data[i]),2);
            }
            Plot bucket_plot({y_data});
            bucket_plot.highlight(squeezing_power);

            int index = squeezing_power / x_spacing;
            ImGui::SameLine();
            ImGui::Text( "%.2f %%", y_data[index] * 100);

            bucket_plot.show_inline(180);
        }   
        ImGui::PushItemWidth(-1);

        // schmidt stuff 
        ImGui::NextColumn();
        ImGui::SetNextItemOpen(true, ImGuiCond_FirstUseEver);
        if(ImGui::CollapsingHeader("Number distribution prob"))
        {
            // static float sq_power = 0.1;
            // ImGui::SliderFloat("", &sq_power, 0, 1);
            int photon_cutoff = 5;
            std::vector<float> number_probs(photon_cutoff,0.0);
            for(int i = 0; i < photon_cutoff; i++)
            {
                number_probs[i] = 100.0 * std::pow(tanh(squeezing_power), 2 * (i)) / std::pow(cosh(squeezing_power), 2);
            }

            double plot_min = 0.0; // 0%
            double plot_max = 50.0; // 50%
            ImGui::PlotHistogram("hist", &number_probs[0], number_probs.size(), 
                    number_probs.size(),  "overlay", plot_min, plot_max, ImVec2(150,100));
            // draw_multi_plot();
        }

        ImGui::SetNextItemOpen(true, ImGuiCond_FirstUseEver);
        if(ImGui::CollapsingHeader("Schmidt modes"))
        {
            ImGui::SameLine();
            ImGui::Text("%d terms", jsa.schmidt.get_num_schmidt_modes());

            /// @TODO need to set jsa_spec size as global so same for all sources 
            if(schmidt_mode_counter >= jsa.schmidt.get_num_schmidt_modes())
            { 
                schmidt_mode_counter = jsa.schmidt.get_num_schmidt_modes() - 1;
            }
            ImGui::Text("Schmidt mode %d", schmidt_mode_counter+1); ImGui::SameLine();
            float spacing = ImGui::GetStyle().ItemInnerSpacing.x;
            ImGui::PushButtonRepeat(true);
            if (ImGui::ArrowButton("##left", ImGuiDir_Left) && schmidt_mode_counter > 0)
            {
                schmidt_mode_counter--;
            }
            ImGui::SameLine(0.0f, spacing);
            if (ImGui::ArrowButton("##right", ImGuiDir_Right) && (schmidt_mode_counter < jsa.schmidt.get_num_schmidt_modes() - 1))
            {
                schmidt_mode_counter++;
            }
            ImGui::PopButtonRepeat();

            ///// singular values 
            ImGui::SetNextItemOpen(true, ImGuiCond_FirstUseEver);
            if(ImGui::TreeNode("Schmidt coeffs"))
            {
                std::vector<float> hist_data(jsa.schmidt.get_num_schmidt_modes(),0.0);

                for(int i = 0; i < jsa.schmidt.get_num_schmidt_modes(); i++)
                {
                    hist_data[i] = jsa.schmidt.S_mode(i);
                }

                double plot_min = 0.0;
                double plot_max = hist_data[0];
                ImGui::SameLine(); ImGui::Text("%f", hist_data[schmidt_mode_counter]);

                ImGui::PlotHistogram("", &hist_data[0], hist_data.size(), 
                        hist_data.size(),  "Schmidt modes", plot_min, plot_max, ImVec2(150,100));
                ImGui::TreePop();
            }

            ImGui::NextColumn();

            // for plots 
            float sign = 1.0;
            ImGui::SetNextItemOpen(true, ImGuiCond_FirstUseEver);
            if(ImGui::TreeNode("Signal modes"))
            {   
                ImGui::Text("U modes"); //ImGui::SameLine();
                int plot_size = jsa.matrix.rows();
                // stuff here
                Eigen::VectorXd x_data(plot_size);//, 0.0);
                Eigen::VectorXd y_data(plot_size);//, 0.0);

                Eigen::VectorXcd vect = jsa.schmidt.U_mode(schmidt_mode_counter);

                if(std::abs(vect.real().minCoeff()) >= std::abs(vect.real().maxCoeff())) 
                {
                    sign = -1.0;
                }
                for(int i = 0; i < plot_size; i++)
                {
                    x_data(i) = i;
                    y_data(i) = sign * std::real(vect(i));
                }

                Plot signal_modes_plot({y_data});
                // signal_modes_plot.set_unit_scale();
                signal_modes_plot.show_inline(180, 100);

                ImGui::TreePop();
            }

            ImGui::SetNextItemOpen(true, ImGuiCond_FirstUseEver);
            if(ImGui::TreeNode("Idler modes"))
            {
                ImGui::Text("V^H modes");// ImGui::SameLine();
                int plot_size = jsa.matrix.rows();
                // stuff here
                Eigen::VectorXd x_data(plot_size);//, 0.0);
                Eigen::VectorXd y_data(plot_size);//, 0.0);

                Eigen::VectorXcd vect = jsa.schmidt.V_mode(schmidt_mode_counter);

                for(int i = 0; i < plot_size; i++)
                {
                    x_data(i) = i;
                    y_data(i) = sign * std::real(vect(i));
                }

                Plot idler_modes_plot({y_data});
                // idler_modes_plot.set_unit_scale();
                idler_modes_plot.show_inline(180, 100);

                ImGui::TreePop();
            }
            // ImGui::NextColumn(); 

            // ImGui::SetNextItemOpen(true, ImGuiCond_FirstUseEver);
            if(ImGui::TreeNode("Source stats"))
            {
                ImGui::BulletText("Source stats");
                ImGui::Indent();
                ImGui::BulletText("Source Purity %f", jsa.schmidt.matrix_schmidt_norm());
                ImGui::Unindent();
                ImGui::TreePop();
            }

        } // end of schmidt decomp


        ImGui::Columns(1);

        ImGui::SetNextItemOpen(true, ImGuiCond_FirstUseEver);
        ImGui::SetNextItemWidth(ImGui::GetContentRegionAvail().x * 0.5f);
        if(ImGui::TreeNode("Save source"))
        {
            ImGui::SetNextItemWidth(200);
            // text input for filename 
            // write out spec info
            static std::string jsa_filename_out;
            //jsa_file.resize(128);
            if(ImGui::InputText("filename to write", &jsa_filename_out))
            {
                // jsa_file.shrink_to_fit();
                std::cout << "name " << jsa_filename_out << " end. Size = " << jsa_filename_out.size() <<  std::endl;
            }
            if(ImGui::Button("Do it!"))
            {
                std::cout << "yeah!" << std::endl;
                std::string path = "./tempplots/";
                // @TODO FIX THIS PROPERLY FOR WINDOWS with std::filesystem
                path ="";
                // write matrix 
                jsa.write(jsa_filename_out);
            }
            ImGui::SetNextItemWidth(-1);
            ImGui::TreePop();
        }
        ImGui::Separator(); 
    }

    Jsa Source_graphics_menu::make_jsa(int spec_size) const 
    {
        return Jsa(spec_size, squeezing_power, func_choice, bandwidth, phasematching, xoffset, yoffset);
    }

    Jsa Source_graphics_menu::make_jsa(int spec_size, double sq_power) const 
    {
        return Jsa(spec_size, sq_power, func_choice, bandwidth, phasematching, xoffset, yoffset);
    }

    Jsa Source_graphics_menu::make_jsa_fromfile(std::string filename)
    {
        return Jsa(filename, squeezing_power);
    }

    bool Source_graphics_menu::changed()
    {
        // if the jsa has changed since last check reset flag and return true
        if(changed_flag) { changed_flag = false; return true;}
        else return changed_flag;
    }

    // -------------------------------------- Filter options --------------------------------
    Filter_graphics_menu::Filter_graphics_menu() 
    {
        update_filter = false;
        reset();
    }

    Filter_graphics_menu::~Filter_graphics_menu() {}

    void Filter_graphics_menu::reset()
    {
        func_choice = "bandpass";
        xoffset = 0;
        yoffset = 0;
        update_filter = true;
        jsa_size = 50;
        filter_width = 25;
        new_window = false;
        filter = make_filter(jsa_size);
    }

    void Filter_graphics_menu::show(int spec_size)
    {
        if(jsa_size != spec_size) 
        {   
            jsa_size = spec_size;
            update_filter = true;
        }
        // static opengl_plot plot;
        // static bool colour_interpolate = false;
        ImGui::Separator();
        ImGui::Text("Filter editor tools");
        // columns 
        ImGui::Columns(4, nullptr, true);
        ImGui::SetColumnWidth(0, 320.0f);
        ImGui::SetColumnWidth(1, 280.0f);
        ImGui::SetColumnWidth(2, 200.0f);

        // static show_me_image jsa_image;
        // check if any of the params change and make a new jsa obj and plot it
        // to show it 
        if(update_filter)
        {
            update_filter = false;
            changed_flag = true;
            filter = make_filter(jsa_size);
            plot.update(filter.matrix.cwiseAbs());
        }

        if(new_window) { plot.show(&new_window); }
        else { plot.show();  }

        ImGui::NextColumn();
        // reset all vals to default 
        float reset_color = 0.0 ;
        ImGui::PushID("reset");

        ImGui::PushStyleColor(ImGuiCol_Button, (ImVec4)ImColor::HSV(reset_color, 0.6f, 0.6f));
        ImGui::PushStyleColor(ImGuiCol_ButtonHovered,(ImVec4)ImColor::HSV(reset_color, 0.7f, 0.7f));
        ImGui::PushStyleColor(ImGuiCol_ButtonActive, (ImVec4)ImColor::HSV(reset_color, 0.8f, 0.8f));

        if(ImGui::Button("Reset", ImVec2(100,20)))
        {
            reset();
        }
        ImGui::PopStyleColor(3);
        ImGui::PopID();

        ImGui::SameLine(); ImGui::Checkbox("new window", &new_window); 
        if(ImGui::Checkbox("Colour interpolation", &plot.interpolate_colours))
        { update_filter = true; } 
        ImGui::SameLine();
        if(ImGui::Checkbox("HSV interp", &plot.use_hsv_interp))
        { update_filter = true; }

        // update_filter |= ImGui::InputDouble("Power", &squeezing_power, 0.01f, 1.0f, "%.8f");
        ImGui::SameLine(); HelpMarker("You can apply arithmetic operators +,*,/ on numerical values.\n  e.g. [ 100 ], input \'*2\', result becomes [ 200 ]\nUse +- to subtract.\n");

        // static std::vector<listbox_item> jsa_funcs_list = {listbox_item("Gaussian", true), listbox_item("Sinc",false)};

        if(ImGui::ListBoxHeader("functions"))
        {
            for(int i = 0; i < (int)filter_funcs_list.size(); i++)
            {
                if(ImGui::Selectable(filter_funcs_list[i].name.c_str(), filter_funcs_list[i].selected))
                {
                    // std::cout << "selected " << filter_funcs_list[i].name << std::endl;
                    for(int j = 0; j < (int)filter_funcs_list.size(); j++)
                    {
                        filter_funcs_list[j].selected = false;
                    }
                    filter_funcs_list[i].selected = true;
                    update_filter |= true;
                    func_choice = filter_funcs_list[i].name;
                }
            }
            ImGui::ListBoxFooter();
        }

        // width
        update_filter |= ImGui::SliderInt("Filter width", &filter_width, 1, jsa_size);       
        ImGui::SameLine(); HelpMarker("CTRL+click to input value.");

        // list for funtions 
        update_filter |= ImGui::SliderDouble("x offset", &xoffset, -3,3,"%.4f" ); 
        ImGui::SameLine(); HelpMarker("CTRL+click to input value.");
        update_filter |= ImGui::SliderDouble("y offset", &yoffset, -3,3,"%.4f");
        ImGui::SameLine(); HelpMarker("CTRL+click to input value.");

        ImGui::NextColumn();

        ImGui::SetNextItemOpen(true, ImGuiCond_FirstUseEver);
        if(ImGui::CollapsingHeader("Filter function"))
        {
            int plot_size = filter.matrix.rows();
            // stuff here
            Eigen::VectorXd y_data(plot_size);//, 0.0);

            for(int i = 0; i < plot_size; i++)
            {
                y_data[i] = std::abs(filter.matrix(i,i));
            }

            Plot filter_plot({y_data});

            filter_plot.show_inline(200);
        }
        ImGui::Columns(1);
        ImGui::Separator(); 
    }

    Filter Filter_graphics_menu::make_filter(int spec_size) const
    {
        return Filter(spec_size, filter_width, func_choice);
    }

    bool Filter_graphics_menu::changed()
    {
        // if the jsa has changed since last check reset flag and return true
        if(changed_flag) { changed_flag = false; return true;}
        else return changed_flag;
    }

    // ------------------------------ optical circuit builder  --------------------
    Circuit::Circuit() 
    {
        num_modes = 4;
        num_components = 8;
        circuit_size = num_components * num_modes;
        // gate_size = (int)gates.size();
        gate_size = gate_labels.size();

        // make the operators, all empty 
        circuit = std::vector<Circuit_op>(circuit_size);//, "\t");
        num_sources = 1;
    }


    void Circuit::save()
    {
        std::ofstream file;
        file.open("circuit.txt");
        file << circuit.size() << "," << num_modes << "," << num_components << std::endl;
        for(size_t i = 0; i < circuit.size(); i++)
        {
            if(!circuit[i].is_empty())
            {
                bool idl_found = (circuit[i].label.find("idl") != std::string::npos);
                    if(!idl_found)
                    {
                        file << circuit[i].print_long() << ";" << std::endl;
                    }
            }
        }
        file.close();
        std::cout << "Saved to circuit.txt" << std::endl;
    }

    void Circuit::load()
    {
        std::ifstream input("circuit.txt");
        // std::istream_iterator<std::string> start(is), end;
        // std::vector<std::string> circuit_in(start, end);
        std::string line;

        int i = 0;
        // int n = 0; // dim of circuit 
        // int num_rows = 0;
        // int num_cols = 0; 

        // std::vector<Circuit_op> circuit_in;
        // get every line of the file and put it into the string "line"
        while(std::getline(input, line))
        {
            // first line gives dim of circuit 
            if(i == 0) 
            {
                // comma separated 
                std::vector<std::string> vals;
                std::stringstream stream(line);
                while( stream.good() )
                {
                    std::string substr;
                    std::getline( stream, substr, ',');
                    vals.push_back(substr);
                }

                circuit_size = std::stoi(vals[0]);
                num_modes = std::stoi(vals[1]);
                num_components = std::stoi(vals[2]);
                // n = std::stoi(vals[0]);
                // num_rows = std::stoi(vals[1]);
                // num_cols = std::stoi(vals[2]);

                if(num_components * num_modes != circuit_size)
                    std::cout << " ERR rows x cols != total n " << std::endl;

                circuit.resize(circuit_size); // make the circuit of size n, default constructor is the empty gate
                // then overwrite any non-zero elements by their position 
                i++;
                continue;
            }


            // use stringstream to process parts of the line to unpack the Circuit_op object
            std::stringstream linestream(line);
            // repeat above for each element in the line separated by the "delimiter" put
            // each into the cell string 
            std::string cell;

            // space separated 

            std::vector<std::string> args; 
            while(std::getline(linestream, cell, ' '))
            {
                args.push_back(cell);
            }
            olistd::print(args);

            // order is
            // name
            // sig / idler // optional only for 2-gates 
            // at position (column number)
            // modes [val] // (row number, 2 elements if 2-gate)
            // ; // ends the line 
            //
            // first is lookup the name to put in the gate 
            // lookup string in the map to get the key enum 

            // typical save 
            //
            // 32,4,8
            // Sq sig at 0 modes [0, 1];
            // Fltr(-1) at 1 modes [0];
            // Sq idl at 0 modes [1, 0];
            // Phase at 2 modes [1];
            // Bs(-1) sig at 0 modes [2, 3];
            // Loss at 1 modes [2];
            /// Bs(-1) idl at 0 modes [3, 2];

            // check if args[0] has bracket in it 
            std::stringstream sstream(args[0]);

            std::vector<std::string> name_vals;
            while(sstream.good())
            {
                std::string name;
                std::getline(sstream, name, '(');
                name_vals.push_back(name);
            }

            Circuit_op op;
            /// check for integer label 
            // search for ; as the label is directly after this and
            // is space terminated 
            size_t semicolon_pos = args[0].find_first_of(";");
            if(semicolon_pos != std::string::npos)
            {
                // found a ;
            std::string  num_label = args[0].substr(semicolon_pos+1, std::string::npos);
            op.num = std::stoi(num_label);
                // the label is from after the semicolon to the end of the first
                // space separated arg 
                name_vals[0] = name_vals[0].substr(0, semicolon_pos);
            }
            // std::cout << " label = " << label << std::endl;
            // std::cout << "name vals ";
            // olistd::print(name_vals);

            // temp circ to write values in 

            Gate key = Gate::Empty;
            for(auto & it : gate_labels)
            {
                if(it.second == name_vals[0])
                {
                    key = it.first;
                    break;
                }
            }

            bool two_mode = false;
            op.name = key;
            if(name_vals.size() > 1) // 2 elements 
            {
                // strip the last ")" from the string and convert to double and set as op.val

                std::string string_val = name_vals[1].substr(0, name_vals[1].size() -1);
                op.sval = string_val;
                op.val = std::stod(string_val);
            }
            if(op.name == Gate::Source || op.name == Gate::Beamsplitter)
            {
                // for modes check the length of the strings 
                // op.label = " " + args[1];
                op.label = " sig";
                op.position = std::stoi(args[2]);

int last_idx = args.size() - 1;

                    int mode1 = std::stoi(args[last_idx - 1].substr(1, args[last_idx - 1].size()));
                    int mode2 = std::stoi(args[last_idx].substr(0, args[last_idx].size() - 1));
                    op.modes = {mode1, mode2};
                    two_mode = true;
            }
            else // single gate
            {
                // for modes check the length of the strings 
                op.position = std::stoi(args[2]);
                    int last_idx = args.size() - 1;
                    int mode1 = std::stoi(args[last_idx].substr(1, args[last_idx].size() - 1));
                op.modes = {mode1};
            }


             // std::cout << op.print_long() << std::endl;
            // then insert the op into the correct place in the vector of circuit_in
            // need to set from row and column vals n[
            // rows, cols  
            int pos = op.modes[0] * num_components + op.position; 
            circuit[pos] = op;
            
            if(two_mode)
                {
                    int second_pos = op.modes[1] * num_components + op.position;
                    auto copy_op(op);
                    std::reverse(copy_op.modes.begin(), copy_op.modes.end());
                    copy_op.label = " idl";
                    copy_op.num = op.num;
                    circuit[second_pos] = copy_op;
                }
        }

        // @TODO FIX THIS 
        // circuit = circuit_in;

        std::cout << "loaded? " << std::endl;
    }

    void Circuit::set_num_modes(int num_modes_in)
    {
        if(num_modes != num_modes_in) 
        {
            circuit_size = num_modes_in * num_components;
            circuit.resize(circuit_size);
            for(int i = 0; i < circuit_size; i++) { circuit[i].reset();}
            // reset mode nums
            num_modes = num_modes_in;
            //    num_components = 8;
        }
    }

    void Circuit::drag_drop()
    {
        // resize the vector if the circuit size changes 
        circuit_size = num_modes * num_components;
        enum Mode
        {
            Mode_Copy,
            Mode_Move,
            Mode_Swap
        };
        static int mode = Mode_Copy;
        //
        // ImGui::Separator(); 
        ImGui::Text("Drag and drop the gates into the circuit");
        // for all the gates and waveguides do stuff
        ImGuiWindowFlags window_flags = ImGuiWindowFlags_HorizontalScrollbar;// | (disable_mouse_wheel ? ImGuiWindowFlags_NoScrollWithMouse : 0);             
        ImGui::BeginChild("Child1", ImVec2(ImGui::GetWindowContentRegionWidth() * 0.95f, 0),        false, window_flags);

        // for all elements in the circuit, wavguides and gates listed above 
        // ImGui::BeginChild("DragDrop", ImVec2(----ImGui::GetItemsLineWidthWithSpacing(),0), false);
        for (int n = 0; n < gate_size; n++)
        {
            ImGui::PushID(n);
            /// Sets how many gates are shown inline 
            int num_gates_on_line = 7;
            if ((n % num_gates_on_line) != 0) 
            {
                ImGui::SameLine();
            }

            float g_hue = 0.75;
            ImGui::PushStyleColor(ImGuiCol_Button, (ImVec4)ImColor::HSV(g_hue, 0.6f, 0.6f));
            ImGui::PushStyleColor(ImGuiCol_ButtonHovered, (ImVec4)ImColor::HSV(g_hue, 0.7f, 0.7f));
            ImGui::PushStyleColor(ImGuiCol_ButtonActive, (ImVec4)ImColor::HSV(g_hue, 0.8f, 0.8f));
            ImGui::Button(gates[n].print().c_str(), ImVec2(90,40));
            ImGui::PopStyleColor(3);
            // all slots can be dragged 
            // Our buttons are both drag sources and drag targets here!
            if (ImGui::BeginDragDropSource(ImGuiDragDropFlags_None))
            {
                ImGui::SetDragDropPayload("GATE_CELL", &gates[n], sizeof(gates[n]));
                // Set payload to carry the index of our item (could be anything)
                if (mode == Mode_Copy)
                { ImGui::Text("Copy %s", gates[n].print().c_str()); }  
                // Display preview (could be anything, e.g. when dragging an image
                // we could decide to display the filename and a small preview 
                // of the image, etc.)
                if (mode == Mode_Move) { ImGui::Text("Move %s", gates[n].print().c_str()); }
                if (mode == Mode_Swap) { ImGui::Text("Swap %s", gates[n].print().c_str()); }
                ImGui::EndDragDropSource();
            }

            ImGui::PopID();
        } // sources go on top row 

        for(int n = 0; n < circuit_size; n++)
        {
            ImGui::PushID(n+gate_size);
            //// ----------------------------- WAVEGUIDES --------------------------
            // if(n >= gate_size) // "waveguides"
            // {
            if(n == 0) { ImGui::Text("Optical circuit");} // ImGui::Separator();}
        // don't ask.
        if (n != 0 && n % (int)((float)circuit_size/(float)num_modes) != 0)
        {
            ImGui::SameLine(); 
        } // same line or not, formatting 
        else 
        {
            int mode_idx = n / num_components;
            // to get the correct spacing for single or double digit modes 
            if(mode_idx < 10)
            ImGui::Text("Mode %d ", mode_idx); 
            else
                ImGui::Text("Mode %d", mode_idx); 

            ImGui::SameLine();
        }
        // mode number for start of each line 

        // if not empty 
        if(circuit[n].is_empty() == false)
        {
            float g_hue = 0.8;
            ImGui::PushStyleColor(ImGuiCol_Button, (ImVec4)ImColor::HSV(g_hue, 0.6f, 0.6f));
            ImGui::PushStyleColor(ImGuiCol_ButtonHovered, (ImVec4)ImColor::HSV(g_hue, 0.7f, 0.7f));
            ImGui::PushStyleColor(ImGuiCol_ButtonActive, (ImVec4)ImColor::HSV(g_hue, 0.8f, 0.8f));
            // non empty waveguides 
            if(ImGui::Button(circuit[n].print().c_str(), ImVec2(100, 40)))
            {
                // if clicked cycle through avaliable sources or different beamsplitter pairs 
                if(circuit[n].has_pair())
                {
                    // update n and its pair 
                    int current = circuit[n].num;
                    int pair = circuit[n].pair_val;

                    if(circuit[n].name == Gate::Source)
                    {
                        if(current >= num_sources)
                        {
                            circuit[n].num = -1;
                            circuit[pair].num = -1;
                        }
                        else 
                        {
                            circuit[n].num++;
                            circuit[pair].num++;
                        }
                    }
                    else
                    {
                        circuit[n].num++;
                        circuit[pair].num++;
                    }
                }
                else // single mode gate 
                {
                    // switch on the name of the circuit element and change what the
                    // increment does to it, i.e 
                    // for loss it could cycle through the values
                    // from 0, 0.1, 0.2, ..., 1
                    //
                    // phase shifter could go in pi/8 steps 


                }
            }
            // after the non-empty waveguides 
            // display stats for op when hovered 
            if(ImGui::IsItemHovered())
            {
                ImGui::BeginTooltip();
                ImGui::PushTextWrapPos(ImGui::GetFontSize() * 35.0f);
                ImGui::TextUnformatted(circuit[n].print_long().c_str());
                ImGui::PopTextWrapPos();
                ImGui::EndTooltip();
            } 
            // right click the operator to set values 
            if (ImGui::BeginPopupContextItem())
            {
                ImGui::Text("Edit name:");
                // ImGui::InputText("###edit", &circuit[n].sval, ImGuiInputTextFlags_EnterReturnsTrue);
                ImGui::PushID(n);
                ImGui::Checkbox("Variable op", &circuit[n].variable);
                // if not variable, most common fixed value case, only show value, else 
                if(circuit[n].variable == false)
                    ImGui::InputDouble("Val", &circuit[n].val);
                else // need to input, start, end, and number of points to evaluate the operator at
                {
                    ImGui::InputDouble("Start val", &circuit[n].start_val);
                    ImGui::InputDouble("End val", &circuit[n].end_val);
                    ImGui::InputDouble("Steps", &circuit[n].steps);
                }

                if (ImGui::Button("Close"))
                {
                    ImGui::CloseCurrentPopup();
                    std::cout << "Exisiting " << circuit[n].print().c_str() << std::endl;
                    std::cout << " update " << std::endl;
                    circuit[n].update_val();

                    if(circuit[n].has_pair())
                    {
                        // update matching pairs value
                        circuit[circuit[n].pair_val].val = circuit[n].val;
                        circuit[circuit[n].pair_val].sval = circuit[n].sval;
                    }
                    std::cout << gate_labels.at(circuit[n].name) << circuit[n].sval << circuit[n].label << std::endl;
                    std::cout << "circ print.c_str " << circuit[n].print().c_str() << std::endl;
                }
                ImGui::PopID();
                ImGui::EndPopup();
            }
            ImGui::PopStyleColor(3);
        } // end of the great waterfall.
        else  // the waveguide is empty 
        {
            if(ImGui::Button(circuit[n].print().data(), ImVec2(100, 40)))
            {
            }
        }
        // Our buttons are both drag sources and drag targets here!
        if (ImGui::BeginDragDropSource(ImGuiDragDropFlags_None))
        {
            ImGui::SetDragDropPayload("WAVEGUIDE_CELL", &n, sizeof(n));   
            // Set payload to carry the index of our item (could be anything)
            if (mode == Mode_Copy) { ImGui::Text("Copy %s", circuit[n].print().c_str()); }    // Display preview (could be anything, e.g. when dragging an image we could decide to display the filename and a small preview of the image, etc.)
            if (mode == Mode_Move) { ImGui::Text("Move %s", circuit[n].print().c_str()); }
            if (mode == Mode_Swap) { ImGui::Text("Swap %s", circuit[n].print().c_str()); }
            ImGui::EndDragDropSource();
        }

        // only "waveguides" are targets
        if (ImGui::BeginDragDropTarget())
        {
            // hovering stuff goes here
            // ImGui::SameLine(); ImGui::Text("something selected"); ImGui::SameLine();
            if (const ImGuiPayload* payload = ImGui::AcceptDragDropPayload("GATE_CELL"))
            {
                // ImGui::Text("something selected");
                // IM_ASSERT(payload->DataSize == sizeof(char));
                // char* payload_n = (char *)payload->Data;
                Circuit_op payload_n = *((Circuit_op *)payload->Data);

                if (mode == Mode_Copy)
                {
                    // std::cout << payload_n << std::endl;
                    if(payload_n.name == Gate::Source || payload_n.name == Gate::Beamsplitter)
                    {
                        if(!(get_row(n) == num_modes -1))  // dont do two mode op
                        {
                            circuit[n] = payload_n;
                            circuit[n].label += " sig";
                            circuit[n].pair_val = n + num_components;
                            circuit[n].position = get_col(n);
                            circuit[n].modes = {get_row(n),get_row(n+num_components)};

                            circuit[n + num_components] = payload_n;
                            circuit[n + num_components].label += " idl";
                            circuit[n + num_components].pair_val = n;
                            circuit[n + num_components].position = get_col(n);
                            circuit[n + num_components].modes = {get_row(n+num_components),get_row(n)};
                        }
                        else{ std::cout << "out of bounds" << std::endl; }
                    }
                    else 
                    { 
                        circuit[n] = payload_n; 
                        circuit[n].position = get_col(n);
                        circuit[n].modes = {get_row(n)};
                    }
                    // erase
                    if(payload_n.is_empty())
                    {
                        // erase target
                        circuit[n] = payload_n;
                        /// TODO should check if upper and lower have the same
                        // label and erase them 
                    }
                }
                if (mode == Mode_Swap)
                {
                    //  const char* tmp = gates[n].c_str();
                    // gates[n] = gates[payload_n];
                    // gates[payload_n] = tmp;
                }
            }
            if (const ImGuiPayload* payload = ImGui::AcceptDragDropPayload("WAVEGUIDE_CELL"))
            {
                std::cout << std::endl;
                /// CONFUSINGLY circuit[n] is the object being dragged 
                //
                // ImGui::Text("something selected");
                // IM_ASSERT(payload->DataSize == sizeof(char));
                int* payload_n = (int *)payload->Data;
                // std::cout << payload_n << std::endl;
                // swap
                // THERE ARE 5 CASES
                // N & PAYLOAD ARE BOTH SINGLE GATES
                // N IS TWO-GATE AND PAYLOAD IS SINGLE
                // N IS SINGLE AND PAYLOAD IS TWO-GATE
                // N & PAYLOAD ARE BOTH TWO-GATES BUT DIFFERENT 
                // N & PAYLOAD ARE BOTH TWO-GATES AND SIGNAL & IDLER PAIR

                // single gate onto single gate case 
                Circuit_op orig_n{circuit[n]};
                Circuit_op orig_payload{circuit[*payload_n]};

                circuit[n] = circuit[*payload_n];
                circuit[n].position = get_col(n);
                circuit[n].modes[0] = get_row(n);

                circuit[*payload_n] = orig_n;
                circuit[*payload_n].position = get_col(*payload_n);
                circuit[*payload_n].modes[0] = get_row(*payload_n);

                // check if has pair and update the pairs position of the n-th object
                // that has just moved 
                if(orig_n.has_pair() && !orig_payload.has_pair())
                {        
                    // std::cout << " n has pair, payload not " << std::endl;
                    // n moved from n to payload, update the pair to reflect this  
                    // n's pair now points to the new n location 
                    // circuit[orig_n.pair_val].modes[1] = circuit[n].modes[0];
                    // orig n -> payload
                    // orig n pair points to payload now
                    circuit[orig_n.pair_val].pair_val = *payload_n;
                    circuit[orig_n.pair_val].modes[1] = circuit[*payload_n].modes[0];
                }
                // otherwise payload had pair 
                else if(!orig_n.has_pair() && orig_payload.has_pair())
                {
                    // std::cout << " payload has pair, n not " << orig_payload.pair_val << std::endl;
                    // std::cout << orig_payload.print_long() << std::endl;
                    // std::cout << circuit[*payload_n].print_long() << std::endl;
                    // std::cout << circuit[n].print_long() << std::endl;

                    // orig paylod -> n
                    // orig payload pair points to n now
                    circuit[orig_payload.pair_val].pair_val = n;
                    circuit[orig_payload.pair_val].modes[1] = circuit[n].modes[0];
                }
                // both have pairs 
                else if(orig_n.has_pair() && orig_payload.has_pair())
                {
                    // std::cout << " both have pairs " << std::endl;
                    // if both halves of the same pair 
                    if(orig_n.pair_val == *payload_n)
                    {
                        // std::cout << " two of the same pair " << std::endl;
                        circuit[orig_n.pair_val].pair_val = n;
                        circuit[orig_payload.pair_val].pair_val = *payload_n;

                        circuit[orig_n.pair_val].modes[1] = circuit[n].modes[0];
                        circuit[orig_payload.pair_val].modes[1] = circuit[*payload_n].modes[0];
                    }
                    else // separate two-gates 
                    {
                        // std::cout << " two different pairs " << std::endl;
                        circuit[orig_n.pair_val].pair_val = *payload_n;
                        circuit[orig_payload.pair_val].pair_val = n;

                        circuit[orig_n.pair_val].modes[1] = circuit[*payload_n].modes[0];
                        circuit[orig_payload.pair_val].modes[1] = circuit[n].modes[0];
                    }
                }
            }
            ImGui::EndDragDropTarget();
            //    }
        }
        ImGui::PopID();
    } // end of for loop
    ImGui::EndChild();

} // great waterfall the second.

// circuit[pos].substr(0,6) == "Source"
std::string Circuit::incr_source(std::string item) 
{
    if(olistd::string_found(item, "Source"))
    {
        // increment source 
        int counter = olistd::get_int_from_string(item);
        if(counter == num_sources) { return item; }
        else { return item.replace(7,1,std::to_string(++counter)); }
    }
    else { return item; }
}  // circuit[pos].substr(0,6) == "Source"

std::string Circuit::deincr_source(std::string item) 
{
    if(olistd::string_found(item, "Source"))
    {
        // increment source 
        int counter = olistd::get_int_from_string(item);
        if(counter == 0) {return item.replace(7,1, "0");}
        else { return item.replace(7,1,std::to_string(--counter)); }
    }
    else
    {
        return item;
    }
}


void Circuit::show(int sources_count)
{
    num_sources = sources_count -1;

    // ImGui::Separator();
    ImGui::SameLine();
    ImGui::Text("Circuit builder tools");
    // Reset button 

    ImGui::SameLine();

    float g_hue = 0.375;
    ImGui::PushStyleColor(ImGuiCol_Button, (ImVec4)ImColor::HSV(g_hue, 0.6f, 0.6f));
    ImGui::PushStyleColor(ImGuiCol_ButtonHovered, (ImVec4)ImColor::HSV(g_hue, 0.7f, 0.7f));
    ImGui::PushStyleColor(ImGuiCol_ButtonActive, (ImVec4)ImColor::HSV(g_hue, 0.8f, 0.8f));

    if(ImGui::Button("SAVE!")) { save(); }
    ImGui::SameLine(); 
    if(ImGui::Button("LOAD!")) { load(); }

    ImGui::PopStyleColor(3);

    float r_hue = 0.0;
    ImGui::PushStyleColor(ImGuiCol_Button, (ImVec4)ImColor::HSV(r_hue, 0.6f, 0.6f));
    ImGui::PushStyleColor(ImGuiCol_ButtonHovered, (ImVec4)ImColor::HSV(r_hue, 0.7f, 0.7f));
    ImGui::PushStyleColor(ImGuiCol_ButtonActive, (ImVec4)ImColor::HSV(r_hue, 0.8f, 0.8f));

    ImGui::SameLine(); if(ImGui::Button("Reset", ImVec2(100,20))) 
    { 
        circuit.resize(circuit_size);
        for(int i = 0; i < circuit_size; i++) { circuit[i].reset();}
        // reset mode nums
        num_modes = 4;
        num_components = 8;
    }
    ImGui::PopStyleColor(3);

    old_modes = num_modes;
    old_components = num_components;

    circuit_size = num_components * num_modes;

    // LEFT COLUYMNS
    // ImGui::Columns(2, nullptr, false);
    // check if number of components change and update the list 
    ImGui::PushItemWidth(200);
    if(ImGui::InputInt("Number of modes", &num_modes)) 
    {
        if(num_modes > old_modes)
        {
            for(int i = 0; i <= num_components; i++) { circuit.push_back({}); }
        }
        else 
        {
            circuit.resize(num_modes * num_components);
        }
        // std::cout << "mode resize ok" << std::endl;
        // if after the resize the circuit block is empty replace with a space 
        for(int i = 0; i < (int)circuit.size(); i++) 
        {
            if(circuit[i].is_empty()) { circuit[i].reset();} 
        }
    } // copy everything and shift rows ;
    // ImGui::NextColumn();
    // RIGHT COLUMN
    ImGui::SameLine();
    if(ImGui::InputInt("Number of operations", &num_components)) 
    {
        if(num_components > old_components)
        {
            for(int i = 0; i < num_modes; i++) { circuit.push_back({}); }
        }
        else 
        {
            circuit.resize(num_modes * num_components);
        }
        // if after the resize the circuit block is empty replace with a space 
        for(int i = 0; i < (int)circuit.size(); i++) 
        {
            if(circuit[i].is_empty()) { circuit[i].reset();} 
        }
    } // copy everything and shift cols ;
    ImGui::PushItemWidth(-1);
    // ImGui::Columns(1);

    // show the drag and drop stuff for the circuit 
    drag_drop();
}


// Circuit parser turns the circuit vector into gates on a state 

/// ----------------------------------- circuit output 
/// takes the circuit str and returns nates with modes they act on 
/// crucially in the correct order!
std::vector<Circuit::string_ints> Circuit::circuit_output()
{
    std::vector<string_ints> results;
    for(int col = 0; col < num_components; col++)
    {
        std::vector<int> rows_ignore;
        for(int row = 0; row < num_modes; row++)
        {
            string_ints output;
            int pos = get_pos(row,col);
            if( (!circuit[pos].is_empty()) && (std::find(rows_ignore.begin(), rows_ignore.end(), row) == rows_ignore.end() )) // write out, if rows is NOT in the ignore list 
            {
                // if single mode write out else
                // check first part of string either Beamsplitter_xyz or
                // Source_xyz
                if(circuit[pos].name == Gate::Beamsplitter
                        || circuit[pos].name == Gate::Source)
                {
                    // check for signal & idler 
                    bool sig_found = (circuit[pos].label.find("sig") != std::string::npos);
                    bool idler_found = (circuit[pos].label.find("idl") != std::string::npos);

                    if(sig_found || idler_found)
                    {
                        // go down all other rows looking for the missing idler or
                        // signal component 
                        for(int rows = row; rows < num_modes; rows++)
                        {
                            // if NOT in ignore list 
                            if(std::find(rows_ignore.begin(), rows_ignore.end(), row) == rows_ignore.end() )
                            {
                                // two mode 
                                int below_pos = get_pos(rows,col);
                                // first row 2 mode ops have to be below 
                                // gates on mode row & row + 1 are the same
                                if(sig_found && (circuit[below_pos].label.find("idl") != std::string::npos) )
                                {
                                    // write out op on mode {row,row+1}
                                    output.modes = {row,rows};
                                    rows_ignore.push_back(row);
                                    rows_ignore.push_back(rows);
                                    rows = num_modes; // end the loop
                                }
                                else if(idler_found && (circuit[below_pos].label.find("sig") != std::string::npos))
                                {
                                    output.modes = {rows, row};
                                    rows_ignore.push_back(row);
                                    rows_ignore.push_back(rows);
                                    rows = num_modes;
                                } // end the loop 
                            }
                        }

                    }
                    // check mode below 
                    output.name = circuit[pos].print();
                }
                else
                {
                    output.name = circuit[pos].print();
                    output.modes = {row};
                }
                results.push_back(output);
            }
        }
    }
    std::cout << "write out components " << std::endl;
    for(int i = 0; i < (int)results.size(); i++)
    {
        std::cout << results[i].name << " modes ";
        for(int j = 0; j < (int)results[i].modes.size(); j++)
        {
            std::cout << results[i].modes[j] << ", ";
        }
        std::cout << std::endl;
    }
    return results;
}


/// ----------------------- Optical circuits, states and measurement results 
Circuit_graphics_menu::Circuit_graphics_menu() {}

void Circuit_graphics_menu::load_covariance_matrix(int & jsa_size)
{
    // sub menu start here for loading covariance matrices 
    if(ImGui::TreeNode("Load a state"))
    {
        ImGui::Checkbox("Load State (Symplectic matrix)", &load_file1);
        ImGui::SameLine();
        // Symplectic_matrix symp_transform(space_dim, spec_dim);

        // int choice = 0;
        if(ImGui::Button("Normal ring cov"))
        {
            space_dim = 8;
            spec_dim = 51;
            jsa_size = spec_dim;
            // symp_transform = Symplectic_matrix{space_dim, spec_dim, willsscale * read_csv("../data/jsas/NormalRingTransform.csv")};
            symp_transform = Symplectic_matrix{willsscale * read_csv("../data/jsas/NormalRingTransform.csv"), space_dim, spec_dim};
            M = State{symp_transform};
            b_update_cov_plot = true;
        }
        ImGui::SameLine();
        if(ImGui::Button("split ring cov"))
        {
            space_dim = 8;
            spec_dim = 51;
            jsa_size = spec_dim;
            // symp_transform = Symplectic_matrix{space_dim, spec_dim, willsscale * read_csv("../data/jsas/SplitRingTransform.csv")};
            symp_transform = Symplectic_matrix{willsscale * read_csv("../data/jsas/SplitRingTransform.csv"), space_dim, spec_dim};
            M = State{symp_transform};
            b_update_cov_plot = true;
        }
        ImGui::SameLine();
        if(ImGui::Button("Idler split ring cov"))
        {
            space_dim = 8;
            spec_dim = 51;
            // symp_transform = Symplectic_matrix{space_dim, spec_dim,willsscale * read_csv("../data/jsas/IdlerSplitRingTransform.csv")};
            symp_transform = Symplectic_matrix{willsscale * read_csv("../data/jsas/IdlerSplitRingTransform.csv"), space_dim, spec_dim};
            jsa_size = spec_dim;
            M = State{symp_transform};
            b_update_cov_plot = true;
        }
        ImGui::SameLine();
        if(ImGui::Button("Vacuum"))
        {
            space_dim = 4;
            spec_dim = jsa_size;
            // symp_transform = Eigen::MatrixXd::Identity(2 *space_dim * spec_dim, 2 * space_dim * spec_dim);
            symp_transform = Symplectic_matrix{space_dim, spec_dim};
            M = State{symp_transform};
            b_update_cov_plot = true;
        }
        //ImGui::SameLine();
        bool set_cov = false; 

        // load from file // assume single spec modes 
        ImGui::SetNextItemWidth(200);
        // text input for filename 
        // write out spec info
        static std::string load_covariance_matrix_filename;
        //jsa_file.resize(128);
        if(ImGui::InputText("Covariance matrix filename", &load_covariance_matrix_filename))
        {
            // jsa_file.shrink_to_fit();
            std::cout << "name " << load_covariance_matrix_filename <<  std::endl;
        }
        ImGui::SameLine();
        ImGui::SetNextItemWidth(200);
        if(ImGui::Button("Load matlab (csv) CM"))
        {
            std::cout << "yeah!" << std::endl;
            std::string path = "../data/covariance_matrices/";
            // @TODO FIX THIS PROPERLY FOR WINDOWS with std::filesystem
            // path ="";
            Eigen::MatrixXcd mat = read_csv(path + load_covariance_matrix_filename);
            int n = mat.rows()/2;
            M = State{n, 1};
            space_dim = n;
            spec_dim = 1;
            jsa_size = spec_dim;
            M.cov.matrix = mat;
            b_update_cov_plot = true;
            set_cov = true;
            M.print();
        }
        ImGui::SameLine();
        ImGui::SetNextItemWidth(200);
        if(ImGui::Button("Load python (numpy) CM"))
        {
            std::cout << "yeah!" << std::endl;
            std::string path = "../data/covariance_matrices/";
            // @TODO FIX THIS PROPERLY FOR WINDOWS with std::filesystem
            // path ="";
            Eigen::MatrixXcd mat = read_python_matrix(path + load_covariance_matrix_filename);
            int n = mat.rows()/2;
            M = State{n, 1};
            space_dim = n;
            spec_dim = 1;
            jsa_size = spec_dim;
            M.cov.matrix = mat;
            b_update_cov_plot = true;
            set_cov = true;
            M.print();
        }

        static double min_squeezing = 0.00001;
        static double max_squeezing = 2;
        ImGui::SetNextItemWidth(250);
        ImGui::SliderDouble("###minsq", &min_squeezing, 0, 10, "Min Squeezing: %f");
        ImGui::SameLine();
        ImGui::SetNextItemWidth(250);
        ImGui::SliderDouble("###maxsq", &max_squeezing, 0, 10, "Max Squeezing: %f");

        static double min_thermal = 0.0;
        static double max_thermal = 2;
        ImGui::SetNextItemWidth(250);
        ImGui::SliderDouble("###mintherm", &min_thermal, 0, 20, "Min Thermal: %f");
        ImGui::SameLine();
        ImGui::SetNextItemWidth(250);
        ImGui::SliderDouble("###maxtherm", &max_thermal, 0, 20, "Max Thermal: %f");

        ImGui::SetNextItemWidth(250);
        static int s = 4;
        ImGui::SliderInt("space", &s, 0, 40);
        ImGui::SameLine();
        if(ImGui::Button("Random"))
        {
            space_dim = s;
            spec_dim = 1;
            jsa_size = spec_dim;

            M = State{s,1};
            M.set_random_state(max_squeezing, max_thermal);
            b_update_cov_plot = true;
            set_cov=true;
        }

        b_update_cov_plot |= ImGui::Button("Show covariance matrix");
        if(b_update_cov_plot)
        {
            // M = State(space_dim,spec_dim);
            // M.cov.matrix = symp_transform * symp_transform.adjoint();
            // M = State{space_dim, spec_dim, symp_transform};
            if(set_cov == false) { M = State{symp_transform}; }

            // std::cout << M.cov.matrix << std::endl;

            plot.update(M.cov.matrix.cwiseAbs() - Eigen::MatrixXd::Identity(M.cov.matrix.rows(), M.cov.matrix.rows()),0, spec_dim );
            // std::cout << " After plot_update for cov " << std::endl;
            b_update_cov_plot = false;
            circuit.set_num_modes(space_dim);
            set_all_modes_detectors();
        }


        ImGui::SetNextItemWidth(250);
        ImGui::SliderInt("##width", &cov_width, 0, 1920, "Width: %d");
        ImGui::SetNextItemWidth(250);
        ImGui::SameLine(); ImGui::SliderInt("##height", &cov_height, 0, 1080, "Height: %d");
        plot.show(cov_width, cov_height);

        // sub menu ends here 
        ImGui::TreePop();
    }
} // end of loading covariance matrices 

void Circuit_graphics_menu::set_up(int & jsa_size)
{
    // spec_dim = jsa_size;
    // Circuit object should not be global.
    // circuit = 

    // static bool prev_load = false;
    // if(load_file1 != prev_load) // Will's matrices are this size.
    // {
    //     space_dim = 8;
    //     spec_dim = 51;
    //     jsa_size = spec_dim;
    //     prev_load = load_file1;
    //     // symp_transform = Symplectic_matrix{space_dim, spec_dim} ;
    //     // M = State{symp_transform};
    // }
    if(!load_file1) // default size for program.
    {
        space_dim = 4;
        spec_dim = jsa_size;
        // symp_transform = Symplectic_matrix{space_dim, spec_dim} ;
        // M = State{symp_transform};
    }
    load_covariance_matrix(jsa_size);

    if(ImGui::Button("set rand basis"))
    {
        set_random_basis();
    }
    ImGui::SameLine();
    if(ImGui::Button("clear basis"))
    {
        clear_meas_basis();
    }
    // pick SET mode 
    // ImGui::SliderInt("###SET mode", &SET_mode, 0, space_dim - 1);
} // end of set_up

bool Circuit_graphics_menu::calc_data(const std::vector<int> & detector_modes)
{
    std::cout << "Bucket detector probabilities (%)" << std::endl;
    M.bucket_detector(detector_modes);


    //    std::cout << "correlation functions" << std::endl;
    M.correlation_functions(detector_modes);

    std::cout << "Number resolved detector probabilities (%)" << std::endl;
    M.pnr_detector(detector_modes, photon_cutoff);
    // update the plots 
    update_bucket_histogram = true;
    update_fock_histogram = true;
    update_correlation_histogram = true;


    std::cout << " finished number and bucket det and correlation funcs " << std::endl;

    update_cov_plot();
    /*
       Meas::Nfolds res = M.bucket_detector(detector_modes); 

       histogram_data.resize(0);
       hist_labels.resize(0);
       for(int i = 0; i < (int)res.size(); i++)
       {
    //std::cout << res[i].modes << ", " << res[i].value << std::endl;
    if(b_show_all_bucket_vals || res[i].value >= CHOP_DOUBLE)
    {
    histogram_data.push_back((float)res[i].value);
    hist_labels.push_back(res[i].modes);

    if(i+1 < (int)res.size())
    {
    if(res[i].modes.size() < res[i+1].modes.size())
    {
    histogram_data.push_back(0.0);
    hist_labels.push_back("");
    }
    }
    } // otherwise hide small values 
    } 
    */
    //
    /*
    // reset the cache 
    if(M.cov.pre_calc_probs.size() != 0)
    {
    M.cov.pre_calc_probs.clear();
    }
    Meas::Nfolds fock_res = M.pnr_detector(detector_modes, photon_cutoff);
    // number resolving probs chart 
    fock_histogram_data.resize(0);
    fock_hist_labels.resize(0);
    for(int i = 0; i < (int)fock_res.size(); i++)
    {
    if(b_remove_vacuum) 
    {
    if(i==0) continue;
    }
    if(b_show_all_fock_vals || fock_res[i].value >= CHOP_DOUBLE)
    {
    //std::cout << res[i].modes << ", " << res[i].value << std::endl;
    fock_histogram_data.push_back((float)fock_res[i].value);
    // fock_hist_labels.push_back(res[i].modes);
    fock_hist_labels.push_back(fock_res[i].click_pattern);

    if(i+1 < (int)fock_res.size())
    {
    if(fock_res[i].modes.size() < fock_res[i+1].modes.size())
    {
    fock_histogram_data.push_back(0.0);
    fock_hist_labels.push_back("");
    }
    }
    } // hide small values 
    }
    */ 

    if(M.get_nspec() > 1)
    {
        ////// covariance matrix BB^H plotting 
        cov_elements_data.resize(detector_modes.size());
        double cov_elements_data_max_scale = 0.0;

        // write data, find max element and scale all other plots to this 
        for(int j = 0; j < (int)cov_elements_data.size(); j++)
        {
            cov_elements_data[j] = get_matrix_blocks(M.cov.matrix, {detector_modes[j]}, M.cov.get_nspec()).cwiseAbs() - Eigen::MatrixXd::Identity(M.cov.get_nspec(), M.cov.get_nspec());

            double mc = cov_elements_data[j].maxCoeff();
            cov_elements_data_max_scale = (cov_elements_data_max_scale >= mc) ? cov_elements_data_max_scale : mc;
        }

        // show different elements of the final covariance matrix 
        cov_plots.resize(cov_elements_data.size());
        for(int i = 0; i < (int)cov_plots.size(); i++)
        {
            cov_plots[i] = opengl_plot{};
            cov_plots[i].update(cov_elements_data[i], cov_elements_data_max_scale);
        }

        // g1 
        g1s.resize(M.cov.get_nspace());
        g1s_max_val = 0.0;
        for(int i = 0; i < M.cov.get_nspace(); i++)
        {
            g1s[i] = M.cov.g1_spec(i);
            g1s_max_val = (g1s_max_val >= g1s[i].maxCoeff()) ? g1s_max_val : g1s[i].maxCoeff();
        }
        // calc g2s 

        if(detector_modes.size() > 1)
        {
            int num_g2s = olistd::binomial_coeff(detector_modes.size(), 2);
            g2s.resize(num_g2s);
            std::vector<std::vector<int> > ncr = olistd::nchooser(detector_modes, 2);

            double g2s_max_scale = 0.0;
            for(int i = 0; i < (int)g2s.size(); i++)
            {
                g2s[i] = M.cov.g2_spec(ncr[i]);
                g2s_max_scale = (g2s_max_scale >= g2s[i].maxCoeff()) ? g2s_max_scale : g2s[i].maxCoeff();
            }

            // show g2 plots scaled to max value of all
            g2_plots.resize(g2s.size());

            for(int j = 0; j < (int)g2_plots.size(); j++)
            {
                g2_plots[j] = opengl_plot{};
                g2_plots[j].update(g2s[j], g2s_max_scale);
                //     g2_plots[j].show(80,100, false); if( j < (int)g2s.size() - 1) { ImGui::SameLine();}
            }
        }
        else 
        {
            // set g2 plot size to 0 as cant do g2s with only a single mode 
            g2_plots.resize(0);
        }
    } // end of spectrally resolved measurements 
    return true;
}

bool Circuit_graphics_menu::calc_number_resolved(const std::vector<int> & detector_modes)
{
    M.pnr_detector(detector_modes, photon_cutoff);
    return true;
}

// sim 
std::vector<int> Circuit_graphics_menu::sim(int jsa_size, const std::vector<Source_graphics_menu> & sources, const std::vector<Filter_graphics_menu> & filters, bool run_flag, double power)
{
    olistd::Timer time1;
    if(!load_file1) 
    { 
        M = State{circuit.get_num_modes(), spec_dim}; 
    } // if no load reset to vacuum


    // else{ M = State{space_dim, spec_dim}; }
    // stimulated_emission_tomography();

    // M = State(circuit.get_num_modes(), jsa_size);
    std::cout << "---------------- START OF SIM ----------------" << std::endl;

    // std::cout << " circuit \n" << std::endl;
    // sort the circuit so that the gates are now in the correct logical order.
    std::vector<Circuit_op> circ_output(circuit.circuit);
    circ_output.erase(std::remove_if(circ_output.begin(), circ_output.end(), [](const Circuit_op & op) { return op.is_empty();}), circ_output.end());

    //for(auto it = circ_output.begin(); it != circ_output.end(); it++)
    //{
    //    std::cout << it->print_long() << std::endl;
    //}

    std::sort(circ_output.begin(), circ_output.end());

    //std::cout << "\n after sortting \n" << std::endl;
    //for(auto it = circ_output.begin(); it != circ_output.end(); it++)
    //{
    //    std::cout << it->print_long() << std::endl;
    //}

    // remove idler as signals contain all the same info 
    circ_output.erase(std::remove_if(circ_output.begin(), circ_output.end(), [](const Circuit_op & op) { return op.is_idler();}), circ_output.end());

    //std::cout << "\n after removing idler parts \n" << std::endl;
    for(auto it = circ_output.begin(); it != circ_output.end(); it++)
    {
        std::cout << it->print_long() << std::endl;
    }


    std::cout << "jsa size = " << jsa_size << std::endl;
    std::cout << "parsed circuit" << std::endl;
    // std::vector<int> detector_modes;
    detector_modes.resize(0);

    for(Circuit_op op : circ_output)
    {
        // now lives in state
        circuit_to_apply(op, M, detector_modes, sources, filters, power, jsa_size);
    }


    // measurement basis is set to something other than "computational" basis
    if(change_basis)
        M.apply(meas_unitary, meas_unitary.modes);

    std::cout << "reached the end of the circuit!" << std::endl;
    M.print();

    // default is to put detectors on all MODES 
    if(detector_modes.size() == 0)
    {
        // populate with numbers 0 to spatial modes -1 
        for(int i = 0; i < M.cov.get_nspace(); i++)
        {
            detector_modes.push_back(i);
        }
    }


    // do data calculation 
    calc_data(detector_modes);
    M.correlation_function(detector_modes);
    // calc_number_resolved(detector_modes);

    std::cout << "\n";
    std::ostringstream out;
    out << time1.stop().count();
    time_taken = out.str();    
    std::cout << "---------------- END ------------------------\n" << std::endl;
    // std::cout << M.disp.vector << std::endl;

    // used to be hist_data.size > 0
    // if(M.b_bucket_uptodate) { data_exists = true; }
    data_exists = true;
    // check_commute();
    // if(M.cov.check_symmetric())
    // { std::cout << "All of abt is symmetric" << std::endl; }
    return detector_modes;
}

// returns true if simulates - any of the output changes otherwise returns false 
bool Circuit_graphics_menu::run(int jsa_size, const std::vector<Source_graphics_menu> & sources, const std::vector<Filter_graphics_menu> & filters, bool run_flag, double power)
{
    bool update = false;
    num_sources = sources.size();    
    ImGui::SetNextItemWidth(250);
    ImGui::SliderInt("###Photon truncation", &photon_cutoff, 0, 5, "Photon cutoff = %d");
    // std::cout << " before SIM" << std::endl;
    if(run_flag)
        std::cout << " Running automaticlly " << std::endl;

    if(ImGui::Button("SIMULATE!") || run_flag)
    {
        std::cout << " Sim " << std::endl;
        // update main 
        // oh my God why have you done this/
        update = true;

        // reset existing containers 
        op_vals.resize(0);
        bucket_vals.resize(0);
        fock_vals.resize(0);
        correlation_vals.resize(0);

        covariance_matrices_variables.resize(0);
        variable_data_exists = false;

        bool no_variables = true;
        // insert step where we check if any of the circuit operations are variable.
        // auto it = circuit.circuit.begin();
        std::vector<int> var_op_loc;

        double varstep;
        for(int i = 0; i < (int)circuit.circuit.size(); i++)
        {
            if(circuit.circuit[i].variable)
            {
                no_variables = false;
                std::cout << " variable op " << std::endl;
                std::cout << circuit.circuit[i].print_long() << std::endl;

                varstep = (circuit.circuit[i].end_val - circuit.circuit[i].start_val) / (double)circuit.circuit[i].steps;

                var_op_loc.push_back(i);
            }
        }

        // loop
        if(no_variables == false)
        {
            for(int j = 0; j <= circuit.circuit[var_op_loc[0]].steps; j++)
            {
                for(int op : var_op_loc)
                {
                    circuit.circuit[op].val = circuit.circuit[op].start_val + j * varstep;
                }
                // then sim 
                std::vector<int> detector_modes = sim(jsa_size, sources, filters, run_flag, power);
                // push the results into a vector 
                op_vals.push_back(circuit.circuit[var_op_loc[0]].val);
                bucket_vals.push_back(M.bucket_detector(detector_modes));
                fock_vals.push_back(M.pnr_detector(detector_modes,1));
                correlation_vals.push_back(M.correlation_function(detector_modes));

                covariance_matrices_variables.push_back(M.cov);
                variable_data_exists = true;
                // std::cout << " bucket " << M.bucket_results[0].value << std::endl;
            }
        }
        std::cout << " finished " << std::endl;
        olistd::print(op_vals);
        /*
           for(int i = 0; i < (int)bucket_vals.size(); i++)
           {
           bucket_vals[i].print(); 
           }
           std::cout << std::endl;
           for(int i = 0; i < (int)correlation_vals.size(); i++)
           {
           correlation_vals[i].print(); 
           }
           std::cout << std::endl;
           for(int i = 0; i < (int)fock_vals.size(); i++)
           {
           fock_vals[i].print();
           }
           std::cout << std::endl;
           */
        if(no_variables)// no variables 
        {

            // someone can figure that out 
            // possibly call in another thread so as not to block,
            sim(jsa_size, sources, filters, run_flag, power);
            // std::cout << "after sim if n_vars " << std::endl;
        }
    }
    // check if async done 

    ImGui::SameLine(); ImGui::Text("Took %s", time_taken.c_str());

    // std::cout << "after timer " << std::endl;

    //std::chrono::milliseconds wait_time(100);
    //if(s1.wait_for(wait_time) != std::future_status::timeout)
    //{
    //    s1.get();
    // end of simulate scope 
    // }
    // else
    //    std::cout << "not ready yet ";

    static std::string haftime;
    static std::string dettime;

    static double haf = 0.0;
    static double detval = 0.0;

    // det vs hafnian testing 
    if(ImGui::TreeNode(" PNR Vs Hafnian calcs "))
    {
        // for all the modes existing in the circuit add a check box to pick a click
        // pattern
        // each check puts true or false and the mode it correposnds to  
        // static std::vector<std::pair<bool, int>> modes_to_select;
        // so,... 
        //cannot convert ‘std::vector<bool>::reference’ {aka ‘std::_Bit_reference’} to ‘bool*’
        // it looks like we are using std::deque!
        // static std::vector<bool> modes_to_select(circuit.get_num_modes(),false);
        // // can't resize dynamically because of the static so make it 10  times larger
        // than you think, probs 100 modes is overkill 
        // @TODO this may well cause problem for you later. 
        const int max_modes = 100;
        static std::deque<bool> modes_to_select(max_modes,false);

        // only have check boxes for the actual number of modes there are 
        for(int mode_count = 0; mode_count < circuit.get_num_modes(); mode_count++)
        {
            std::string name = "Mode " + std::to_string(mode_count);
            // apart from the first one keep them on the sameline 
            // ImGui::SetNextItemWidth(400);
            // spacing so that the mode checkoxes line up with photon nums below
            // float spacing = ImGui::GetStyle().ItemInnerSpacing.x;
            float spacing = 25;
            if(mode_count != 0)
                ImGui::SameLine(0.0f, spacing);
            ImGui::Checkbox(name.c_str(), &modes_to_select[mode_count]);
        }



        // same as above but now need ints for the number of photons in each mode 
        static std::vector<int> pnr_numbers(max_modes, 0);

        for(int mode_count = 0; mode_count < circuit.get_num_modes(); mode_count++)
        {
            std::string name = "##Num " + std::to_string(mode_count);
            if(mode_count != 0)
                ImGui::SameLine();
            ImGui::SetNextItemWidth(80);
            ImGui::InputInt(name.c_str(), &pnr_numbers[mode_count]);
        }
        if(ImGui::Button(" check selected modes "))
        {
            // olistd::print(modes_to_select);
            // // don't print the whole of the list only show for the number of modes in
            // the circuit 
            for(auto it = modes_to_select.begin(); it != modes_to_select.begin() + circuit.get_num_modes(); ++it)
            {
                std::cout << " " << *it;
            }
            std::cout << std::endl;
        }
        //ImGui::SameLine();

        // have a input (int) box for photon number in each mode 

        if(ImGui::Button("PNR Det vs Hafnian"))
        {
            std::vector<int> pnr_modes;
            std::vector<int> pnr_nums;
            // check through the list of modes and make a vector of ints of the positions
            // where the deque is true, ignore all positions for false 
            for(int i = 0; i < circuit.get_num_modes(); i++)
            {
                if(modes_to_select[i])
                {
                    pnr_modes.push_back(i);
                    pnr_nums.push_back(pnr_numbers[i]);
                }
            }


            // std::vector<int> pnr_modes = {0,1,2,3};
            // std::vector<int> pnr_nums = {1,1,1,1};

            // std::vector<int> pnr_modes = {0,1};
            // std::vector<int> pnr_nums = {1,1};

            // std::vector<int> pnr_modes = {0};
            // std::vector<int> pnr_nums = {1};


            olistd::Timer timehaf;
            haf = M.pnr_detector_hafnian(pnr_modes, pnr_nums).value();
            haftime = timehaf.get_time();

            M.pre_calc_probs.clear(); // make sure no caching 

            olistd::Timer timedet;
            detval = M.pnr_detector(pnr_modes, pnr_nums).value();
            dettime = timedet.get_time();
        }

        ImGui::Text("Haf val = %f", haf);
        ImGui::SameLine(); ImGui::Text("Hafnian Took %s", haftime.c_str());

        ImGui::Text("det val = %f", detval);
        ImGui::SameLine(); ImGui::Text("det Took %s", dettime.c_str());

        ImGui::TreePop();
    }
    // std::cout << " before return update " << std::endl;

    ImGui::SetNextItemWidth(200);
    ImGui::PushItemWidth(-1);
    return update; // true if sim is selected 
} 

void Circuit_graphics_menu::show_circuit()
{
    ///////// ----------------------- Show the circuit menu 
    circuit.show(num_sources);
}

// ------------------------------------- End of circuit show 
//    ImGui::Separator();
void Circuit_graphics_menu::show_covariance_plots()
{
    // ImGui::SetNextItemOpen(true, ImGuiCond_FirstUseEver);
    if(M.get_nspec() > 1)
    {
        if(ImGui::TreeNode("Show mode state") )
        {
            // show different elements of the final covariance matrix 
            for(int i = 0; i < (int)cov_plots.size(); i++)
            {
                cov_plots[i].show(80,120, false); 
                if(i < (int)cov_plots.size()-1)
                {
                    ImGui::SameLine();
                }
            }

            ImGui::TreePop();
        }
    }
}

void histogram_from_Nfolds(const Meas::Nfolds & nfolds, std::string label, float histogram_width)
{
}



void Circuit_graphics_menu::show_bucket_plots(const Meas::Nfolds & res)
{
    ImGui::SetNextItemOpen(true, ImGuiCond_FirstUseEver);
    if(ImGui::TreeNode("Show bucket detector probabilities"))
    {
        // for the histogram axes scaling 
        // show histogram 
        if(data_exists && !res.is_empty())
        {
            static Histograms_from_Nfolds bucket_histogram{res, "bucket", 550};

            bucket_histogram.update(res, "Bucket", 550);
            /*
            // true first time to set values 
            if(update_bucket_histogram) 
            {
            bucket_histogram_data.resize(0);
            bucket_hist_labels.resize(0);

            // turns Meas::Nfolds into vector of floats and vector of strings for labels 


            // Meas::Nfolds res = M.bucket_detector(detector_modes); 
            for(int i = 0; i < (int)res.size(); i++)
            {
            //std::cout << res[i].modes << ", " << res[i].value << std::endl;
            if(b_show_all_bucket_vals || abs(res[i].value) >= CHOP_DOUBLE)
            {
            bucket_histogram_data.push_back((float)res[i].value);
            bucket_hist_labels.push_back(res[i].modes);

            if(i+1 < (int)res.size())
            {
            if(res[i].modes.size() < res[i+1].modes.size())
            {
            bucket_histogram_data.push_back(0.0);
            bucket_hist_labels.push_back("");
            }
            }
            } // otherwise hide small values 
            }

            double val = *std::max_element(bucket_histogram_data.begin(), bucket_histogram_data.end());
            bucket_max_val = bucket_max_val < val ? val : bucket_max_val;
            // max_val = val;
            update_bucket_histogram = false;
            }
            ImGui::PlotHistogram("##buck", &bucket_histogram_data[0], bucket_histogram_data.size(), 
            bucket_histogram_data.size(),  "Bucket click Prob", 0.0f, bucket_max_val, ImVec2(histogram_width,150));

            float spacing = (float)histogram_width / (float)(bucket_histogram_data.size()+0);
            ImGui::Text(" ");
            for(int i = 0; i < (int)bucket_hist_labels.size(); i++)
            {
            ImGui::SameLine((i+1.0)*spacing); ImGui::Text("%s",bucket_hist_labels[i].c_str());
            }

            if(ImGui::Button("fix scaling"))
            {
            bucket_max_val = *std::max_element(bucket_histogram_data.begin(), bucket_histogram_data.end());
            update_bucket_histogram = true;
            }
            ImGui::SameLine();
            if(ImGui::Button("hide small vals"))
            {
            b_show_all_bucket_vals = !b_show_all_bucket_vals;
            update_bucket_histogram = true;
            }
            */
        }
        ImGui::TreePop();
    }
    // end of bucket probs 
}

void Circuit_graphics_menu::show_fock_plots(const Meas::Nfolds & fock_res)
{
    // number resolved 
    ImGui::SetNextItemOpen(true, ImGuiCond_FirstUseEver);
    if(ImGui::TreeNode("Show Fock probabilities"))
    {
        if(data_exists && !fock_res.is_empty())
        {
            std::cout << "fock_res.size() = " << fock_res.size() << std::endl;
            std::cout << "fock res is_empty = " << fock_res.is_empty() << std::endl;
            fock_res.print();

            static Histograms_from_Nfolds fock_histogram{fock_res, "Number", 550};
            fock_histogram.update(fock_res, "Number", 550); 
            /*
            // for the histogram axes scaling 
            // show histogram 
            // if there is data, display it 
            if(update_fock_histogram)
            {
            // reset the cache 
            if(M.cov.pre_calc_probs.size() != 0)
            {
            M.cov.pre_calc_probs.clear();
            }


            // Meas::Nfolds fock_res = M.pnr_detector(detector_modes, photon_cutoff);
            // number resolving probs chart 
            fock_histogram_data.resize(0);
            fock_hist_labels.resize(0);
            for(int i = 0; i < (int)fock_res.size(); i++)
            {
            if(b_remove_vacuum) 
            {
            if(i==0) continue;
            }
            if(b_show_all_fock_vals || fock_res[i].value >= CHOP_DOUBLE)
            {
            //std::cout << res[i].modes << ", " << res[i].value << std::endl;
            fock_histogram_data.push_back((float)fock_res[i].value);
            // fock_hist_labels.push_back(res[i].modes);
            fock_hist_labels.push_back(fock_res[i].click_pattern);

            if(i+1 < (int)fock_res.size())
            {
            if(fock_res[i].modes.size() < fock_res[i+1].modes.size())
            {
            fock_histogram_data.push_back(0.0);
            fock_hist_labels.push_back("");
            }
            }
            } // hide small values 
            }

            // olistd::print(fock_histogram_data);
            double val = *std::max_element(fock_histogram_data.begin(), fock_histogram_data.end());
            fock_max_val = fock_max_val < val ? val : fock_max_val;
            update_fock_histogram = false;
            }
            ImGui::PlotHistogram("###fock", &fock_histogram_data[0], fock_histogram_data.size(), 
            fock_histogram_data.size(),  "Fock Prob", 0.0f, fock_max_val, ImVec2(histogram_width,150));

            float spacing = (float)histogram_width / (float)(fock_histogram_data.size()+0);
            ImGui::Text(" ");
            for(int i = 0; i < (int)fock_hist_labels.size(); i++)
            {
            ImGui::SameLine((i+1.0)*spacing); ImGui::Text("%s",fock_hist_labels[i].c_str());
            }

            if(ImGui::Button("fix scaling"))
            {
            fock_max_val = *std::max_element(fock_histogram_data.begin(), fock_histogram_data.end());
            update_fock_histogram = true;
            }
            ImGui::SameLine();
            if(ImGui::Button("remove vacuum"))
            {
            b_remove_vacuum = !b_remove_vacuum;
            update_fock_histogram = true;
            }
            ImGui::SameLine();
            if(ImGui::Button("hide small vals"))
            {
            b_show_all_fock_vals = !b_show_all_fock_vals;
            update_fock_histogram = true;
        }
        */
        }
        ImGui::TreePop();
    }
}

// correlation functions 
void Circuit_graphics_menu::show_correlation_plots(const Meas::Nfolds & res)
{
    ImGui::SetNextItemOpen(true, ImGuiCond_FirstUseEver);
    if(ImGui::TreeNode("Show correlation function probabilities"))
    {
        // std::cout << " cor data size = " << res.size() << std::endl;
        // for the histogram axes scaling 
        // show histogram 
        if(data_exists && !res.is_empty())
        {
            static Histograms_from_Nfolds correlation_histogram{res, "Correlation", 550};
            correlation_histogram.update(res, "Correlation", 550);

            /*
            // true first time to set values 
            if(update_correlation_histogram) 
            {
            correlation_histogram_data.resize(0);
            correlation_hist_labels.resize(0);
            // Meas::Nfolds res = M.bucket_detector(detector_modes); 
            for(int i = 0; i < (int)res.size(); i++)
            {
            //std::cout << res[i].modes << ", " << res[i].value << std::endl;
            if(b_show_all_correlation_vals || res[i].value >= CHOP_DOUBLE)
            {
            correlation_histogram_data.push_back((float)res[i].value);
            correlation_hist_labels.push_back(res[i].modes);

            if(i+1 < (int)res.size())
            {
            if(res[i].modes.size() < res[i+1].modes.size())
            {
            correlation_histogram_data.push_back(0.0);
            correlation_hist_labels.push_back("");
            }
            }
            } // otherwise hide small values 
            }

            double val = *std::max_element(correlation_histogram_data.begin(), correlation_histogram_data.end());
            correlation_max_val = correlation_max_val < val ? val : correlation_max_val;
            // max_val = val;
            update_correlation_histogram = false;
            }
            ImGui::PlotHistogram("##cor", &correlation_histogram_data[0], correlation_histogram_data.size(), correlation_histogram_data.size(),  "Correlation functions", 0.0f, correlation_max_val, ImVec2(histogram_width,150));

            float spacing = (float)histogram_width / (float)(bucket_histogram_data.size()+0);
            ImGui::Text(" ");
            for(int i = 0; i < (int)correlation_hist_labels.size(); i++)
            {
            ImGui::SameLine((i+1.0)*spacing); ImGui::Text("%s", correlation_hist_labels[i].c_str());
            }

            if(ImGui::Button("fix scaling"))
            {
            correlation_max_val = *std::max_element(correlation_histogram_data.begin(), correlation_histogram_data.end());
            update_correlation_histogram = true;
            }
            ImGui::SameLine();
            if(ImGui::Button("hide small vals"))
            {
            b_show_all_correlation_vals = !b_show_all_correlation_vals;
            update_correlation_histogram = true;
            }
            */
        }
        ImGui::TreePop();
    }
    // end of correlation function plots
}

void Circuit_graphics_menu::show_plots()
{
    if(op_vals.size() > 0)
    {
        static bool animate = true;
        ImGui::Checkbox("Animate", &animate);
        ImGui::SameLine();
        static float framerate = 1.0f;
        ImGui::SetNextItemWidth(200);
        ImGui::SliderFloat("##Framerate", &framerate, 1.0, 60.0, "Framerate: %f");
        static double refresh_time = 0.0;

        if(!animate || refresh_time == 0.0)
            refresh_time = ImGui::GetTime();

        static int counter = 0;
        static int s = +1;
        while(refresh_time < ImGui::GetTime())
        {
            counter += s;
            if(counter == 0 || counter == ((int)op_vals.size() -1))
                s = -s;

            refresh_time += framerate / 60.0f;
        }

        ImGui::SliderInt("###counter", &counter, 0, (int)op_vals.size()-1, "Counter = %d");
        update_bucket_histogram = true;
        update_fock_histogram = true;
        update_correlation_histogram = true;
        show_bucket_plots(bucket_vals[counter]);
        show_correlation_plots(correlation_vals[counter]);
        show_fock_plots(fock_vals[counter]);

        update_cov_plot(covariance_matrices_variables[counter]);
        show_cov();
    }
    else // default 
    {
        // std::cout << "DB before show cov" << std::endl;
        show_cov();
        //std::cout << "DB before show bucket" << std::endl;
        show_bucket_plots();
        //std::cout << "DB before show correlation" << std::endl;
        show_correlation_plots();
        // std::cout << "DB before show fock" << std::endl;
        show_fock_plots();
    }

    // std::cout << "DB after first block in show_plots" << std::endl;

    // g1 s
    // ImGui::SetNextItemOpen(true, ImGuiCond_FirstUseEver);
    if(ImGui::TreeNode("Show spectrally resolved g1") && M.get_nspec() > 1)
    {
        // show plots
        for(int j = 0; j < (int)g1s.size(); j++)
        {
            ImGui::PushID(j);
            Plot g1_plot({g1s[j]});
            g1_plot.show_inline(180);

            if(j < (int)g1s.size() -1) {ImGui::SameLine();}

            ImGui::PopID();
        }
        ImGui::TreePop();
    }

    // g22 s
    // ImGui::SetNextItemOpen(true, ImGuiCond_FirstUseEver);
    if(ImGui::TreeNode("Show spectrally resolved g2") && M.get_nspec() > 1)
    { 
        // show plots
        for(int j = 0; j < (int)g2_plots.size(); j++)
        {
            g2_plots[j].show(80,100, false); 
            if( j < (int)g2_plots.size() - 1) 
            { ImGui::SameLine();}
        }
        ImGui::TreePop();
    }

    // show variables if exists 
    show_variable_plots();
    // std::cout << "DB end of show_plots " << std::endl;
}// end of show_plots()

void Circuit_graphics_menu::show_variable_plots()
{
    if(variable_data_exists)
    {
        /*
           static bool hide_small_vals = true;
           if(ImGui::Button("Hide small probabilities"))
           hide_small_vals = !hide_small_vals;

        // if less than this hide it 
        double epsilon = 1e-6;

        int data_size = op_vals.size();

        struct bool_int {
        bool b = false;
        int value = 0;
        }; 

        static std::vector<bool_int> to_show(data_size);

        std::vector<Eigen::VectorXd> bucket_data;
        std::vector<Eigen::VectorXd> fock_data;
        std::vector<Eigen::VectorXd> correlation_data;

        // for each y data set
        Eigen::VectorXd op_data(data_size);

        std::vector<float> fvec(op_vals.begin(), op_vals.end());
        // bucket probs 
        for(int j = 0; j < (int)bucket_vals.size(); j++)
        {
        Eigen::VectorXd temp_bucket_data(data_size);
        int i = 0;
        for(const auto & row : bucket_vals[j].nfolds)
        {
        for(const auto & col : row)
        {
        temp_bucket_data(i) = col.value;
        i++;
        }
        }
        if(hide_small_vals == false || temp_bucket_data.maxCoeff() > epsilon)
        bucket_data.push_back(temp_bucket_data);
        }

        // correlation functions  
        for(int j = 0; j < (int)correlation_vals[0].nfolds.size(); j++)
        {
        Eigen::VectorXd temp_correlation_data(data_size);
        int i = 0;
        for(const auto & row : correlation_vals[j].nfolds)
        {
        for(const auto & col : row)
        {
        temp_correlation_data(i) = col.value;
        i++;
        }
        }
        if(hide_small_vals == false || temp_correlation_data.maxCoeff() > epsilon)
        correlation_data.push_back(temp_correlation_data);
        }
        // fock probs 
        // start from 1 to ignore vacuum
        for(int j = 1; j < (int)fock_vals[0].nfolds.size(); j++)
        {
        Eigen::VectorXd temp_fock_data(data_size);
        int i = 0;
        for(const auto & row : fock_vals[j].nfolds)
        {
        for(const auto & col : row)
        {
        temp_fock_data(i) = col.value;
        i++;
        }
    }
    // if show eeverything OR is bigger than the cutoff include it 
    if(hide_small_vals == false || temp_fock_data.maxCoeff() > epsilon)
        fock_data.push_back(temp_fock_data);
    }

    Plot variable_measurements_plot_bucket(bucket_data);
    variable_measurements_plot_bucket.xdata(&fvec[0]);
    variable_measurements_plot_bucket.show_inline(400,400);

    ImGui::SameLine();
    Plot variable_measurements_plot_correlation(correlation_data);
    variable_measurements_plot_correlation.xdata(&fvec[0]);
    variable_measurements_plot_correlation.show_inline(400,400);

    ImGui::SameLine();
    Plot variable_measurements_plot_fock(fock_data);
    variable_measurements_plot_fock.xdata(&fvec[0]);
    variable_measurements_plot_fock.show_inline(400,400);

    ImGui::SameLine();
    // for all the data points 
    for(int i = 0; i < data_size; i++)
    {
        ImGui::PushID(i);

        ImGui::Checkbox("###a", &to_show[i].b);

        ImGui::PopID();

    }
    */
    }
}

void Circuit_graphics_menu::update_cov_plot()
{
    update_cov_plot(M.cov);
}

void Circuit_graphics_menu::update_cov_plot(const Covariance_matrix & cov)
{
    // normally ordered Symplectic basis 
    std::vector<int> normally_ordered( 2 * detector_modes.size() ); 
    std::vector<int> mode_grouped_basis( 2 * detector_modes.size() );
    for(size_t i = 0; i < detector_modes.size(); i++)
    {
        normally_ordered[i] = detector_modes[i];
        normally_ordered[detector_modes.size() + i] = detector_modes[i] + M.get_nspace();

        mode_grouped_basis[2 * i] = detector_modes[i];
        mode_grouped_basis[2 * i + 1] = detector_modes[i] + M.get_nspace();
    }

    Eigen::MatrixXd mat;
    if(normal_basis)
        mat = get_matrix_blocks(cov.matrix, normally_ordered, M.get_nspec()).cwiseAbs();
    else 
        mat = get_matrix_blocks(cov.matrix, mode_grouped_basis, M.get_nspec()).cwiseAbs();

    cov_plot.update(mat - Eigen::MatrixXd::Identity(mat.rows(), mat.rows()), 0, M.get_nspec());
    // std::cout << mat << std::endl;
}

void Circuit_graphics_menu::show_cov()
{
    if(ImGui::Checkbox("Normal basis", &normal_basis))
    {
        mode_basis = !mode_basis;
        update_cov_plot();
    }
    ImGui::SameLine();
    if(ImGui::Checkbox("Covaraince basis", &mode_basis))
    {
        normal_basis = !normal_basis;
        update_cov_plot();
    }

    ImGui::SetNextItemWidth(200);
    ImGui::SliderInt("width", &cov_plot_width, 0 ,1920);
    ImGui::SameLine(); 
    ImGui::SetNextItemWidth(200);
    ImGui::SliderInt("height", &cov_plot_height, 0, 1080);
    cov_plot.show(cov_plot_width, cov_plot_height);
}


// save outputs to files 
void Circuit_graphics_menu::save_output()
{
    // do stuff 
    //jsa_file.resize(128);
    // ImGui::Text("HELLLLLLLLLLLLLLLLLLLLLLLLLLO");
    ImGui::SetNextItemWidth(200);
    if(ImGui::InputText("filename to write", &filename_out))
    {
        std::cout << "name " << filename_out << " end. Size = " << filename_out.size() <<  std::endl;
    }
    ImGui::NextColumn();
    if(ImGui::Button("Do it!"))
    {
        std::cout << "yeah!" << std::endl;
        std::string path = "./tempplots/";
        // @TODO FIX FOR WINDOWS 
        path = "../data/covariance_matrices/";
        // write matrix 
        std::string filename = path + filename_out;

        Eigen::MatrixXcd ident = Eigen::MatrixXcd::Identity(M.cov.matrix.rows(), M.cov.matrix.cols());
        Eigen::MatrixXcd cov_minus_vacuum = M.cov.matrix - ident;
        // write covariance matrix 
        write_file(cov_minus_vacuum, filename + "cov" + ".m");
        write_file(cov_minus_vacuum, filename + "cov_mag" + ".m", "abs");
        write_file(cov_minus_vacuum, filename + "cov_phase" + ".m", "phase");

        // write statistics 
        // nfolds 
        // bucket 
        // number resolved
        // correlation funcs

        /*
           for(int i = 0; i < (int)cov_elements_data.size(); i++)
           {
           write_file(cov_elements_data[i], path + filename_out + "cov" + std::to_string(i) + "m.dat");
           }

        // g2 spec resolved 
        for(int i = 0; i < (int)g2s.size(); i++)
        {
        write_file(g2s[i], path + filename_out + "g2" + std::to_string(i) + "m.dat");
        }


        // g2 spec resolved 
        for(int i = 0; i < (int)g1s.size(); i++)
        {
        write_file(g1s[i], path + filename_out + "g1" + std::to_string(i) + "s.dat");
        }
        */
    }
    ImGui::SetNextItemWidth(-1);
}


// functions not in a class  but circuit parsing 
// apply operators 
void circuit_to_apply(const Circuit_op & op, State & M, std::vector<int> & detector_modes, const std::vector<Source_graphics_menu> & sources, const std::vector<Filter_graphics_menu> & filters, const double power, const int jsa_size)
{
    // lookup table for circuit strings and objects 
    /// THE GRAND SWITCH STATEMENT 
    //// Should we set default args here i.e. if val is not set its value is -1
    //,should I pick defaults for beamsplitter here? 
    switch(op.name)
    {
        case Gate::Source:
            {
                int squ = (op.num == -1) ? 0 : op.num;
                // std::cout << " source number " << squ << std::endl;
                // probs should be .make_jsa()
                if(power == -1 && op.val == -1)
                {
                    M.apply(Active_unitary(sources[squ].make_jsa(jsa_size)), op.modes);
                }
                else
                {
                    M.apply(Active_unitary(sources[squ].make_jsa(jsa_size, op.val)), op.modes);
                }
                break;
            }
        case Gate::Singlesource:
            {
                int squ = (op.num == -1) ? 0 : op.num;
                if(power == -1 && op.val == -1)
                {
                    M.apply(Single_mode_squeezer{sources[squ].make_jsa(jsa_size)}, op.modes);
                }
                else
                {
                    M.apply(Single_mode_squeezer{sources[squ].make_jsa(jsa_size, op.val)}, op.modes);
                }

                break;
            }
        case Gate::Beamsplitter:
            {
                // if op.val not set default to 50:50 bs
                if(op.val == -1)
                    M.apply(Beam_splitter(), op.modes);
                else 
                    M.apply(Beam_splitter(QGot::Reflectivity{op.val}), op.modes);
                break;
            }
        case Gate::Phase:
            {
                // if op.val not set do it anyway
                M.apply(Phase_shift(op.val), op.modes);
                break;
            }
        case Gate::Loss:
            {
                // if op.val not set do no loss
                if(op.val < 0)
                    M.apply(Loss(0.0), op.modes);
                else
                    M.apply(Loss(op.val), op.modes);
                break;
            }
        case Gate::Filter:
            {
                // if no number use the first filter in the vector of filter objects 
                int filt = (op.num == -1) ? 0 : op.num;
                M.apply(filters[filt].make_filter(jsa_size), op.modes);
                break;
            }
        case Gate::Thermal:
            {
                M.set_thermal({op.val}, op.modes);

                break;
            }
        case Gate::Click_Detector:
            {
                // click detectors don't take arguments
                detector_modes.push_back(op.modes[0]);
                break;
            }
        case Gate::Vacuum_Detector:
            {
                // add to the list off forced vacuum modes 
                // do stuff :)
                break;
            }
        case Gate::PNR_Detector:
            {
                // do other stuff 
                // could take number of photons as op.val 
                detector_modes.push_back(op.modes[0]);
                break;
            }
        case Gate::Empty:
            {
                // sorting above should have removed all the empty operations. oops
                std::cout << " Applying Identity " << std::endl;
                break;
            }
            // don't have a default when using scoped enums as the compiller (g++ atleast) warns you
            // if members of the enum are not handled in the switch statement 
            // default:
            //    std::cout << "not a valid operation" << std::endl;
            //    break;
    } 
}




void Ring_graphics_menu::set_up()
{
    ImGui::Columns(2);
    bool b_update_ring = false;
    b_update_ring |= ImGui::RadioButton("Std 2 Ring", &ring_sys_picker, 0);
    ImGui::SameLine();
    b_update_ring |= ImGui::RadioButton("Backscatter 2 Ring", &ring_sys_picker, 1); 

    ImGui::Checkbox("Auto update symplectic transform", &b_auto_update_symp);

    // ImGui::SameLine();

    if(b_update_ring)
    {
        set_initial_vals();
        update_ring |= true;
        b_update_ring = false;
    }

    ImGui::PushItemWidth(200);
    update_ring |= ImGui::SliderInt("Ring spec", &ring_spec, 1, 250, "Resolution = %d"); 

    // std::cout << " params " << std::endl;
    // std::cout << " ring sys picker " << ring_sys_picker <<  std::endl;
    if(ring_sys_picker == 0) // default case
    {
        update_ring |= ImGui::SliderDouble("k1", &params[0], 0.0, 1.0,"k1 = %f"); 
        ImGui::SameLine();
        update_ring |= ImGui::SliderDouble("k2", &params[1], 0.0, 1.0, "k2 = %f"); 
        ImGui::SameLine();
        update_ring |= ImGui::SliderDouble("ka1", &params[2], 0.0, 1.0, "ka1 = %f");
        ImGui::SameLine();
        update_ring |= ImGui::SliderDouble("ka2", &params[3], 0.0, 1.0, "ka2 = %f");
        n_ring = 2;
        n_bus = 3;
        // std::cout << " sliders " << std::endl;
    }
    else if(ring_sys_picker == 1) // backscatter 
    {
        update_ring |= ImGui::SliderDouble("k1", &params[0], 0.0, 1.0,"k1 = %f");
        ImGui::SameLine();
        update_ring |= ImGui::SliderDouble("kl", &params[1], 0.0, 1.0, "kl = %f");
        ImGui::SameLine();
        update_ring |= ImGui::SliderDouble("kbc", &params[2], 0.0, 1.0, "kbc = %f"); 
        ImGui::SameLine();
        update_ring |= ImGui::SliderDouble("kbs", &params[3], 0.0, 1.0, "kbs = %f");
        //  ImGui::SameLine();
        update_ring |= ImGui::SliderDouble("kphi", &params[4], 0.0, 1.0, "kphi = %f");
    }

    // next col
    ImGui::NextColumn();

    show_pic();
    ImGui::Columns(1);
    ImGui::PushItemWidth(-1);
}

void Ring_graphics_menu::set_initial_vals()
{
    if(ring_sys_picker == 0) // 2 ring
    {
        n_ring = 2;
        n_bus = 3;
        params = std_params;
    }
    else if(ring_sys_picker == 1) // backscatter 
    {
        n_ring = 2; 
        n_bus = 4; 
        params = backscatter_params;
    }
}

void Ring_graphics_menu::run()
{
    ImGui::PushItemWidth(200);

    if(ImGui::Button("Make ring") || update_ring)
    {
        olistd::Timer time_ring;
        ring = Ring_resonator(n_ring, n_bus, ring_spec, params, ring_sys_picker);

        ring.doitall();

        ring_plot.update(ring.jsa.matrix.cwiseAbs(), 0, ring_spec);
        update_ring = false;

        calc_transmission_powers();

        time_taken = time_ring.get_time();
    }

    ImGui::SameLine(); ImGui::Text("Took %s", time_taken.c_str());
    ImGui::PushItemWidth(-1);
}

void Ring_graphics_menu::show()
{
    ImGui::PushItemWidth(200);

    ImGui::SliderInt("width", &ring_width, 0, 1920, "Width = %d");
    ImGui::SameLine(); 
    ImGui::SliderInt("height", &ring_height, 0, 1080, "Height = %d");

    ImGui::Separator();
    ring_plot.show(ring_width, ring_height);

    //ImGui::Columns(2,"a");
    // text input for filename 
    // write out spec info

    ImGui::PushItemWidth(-1);
}// end of show 

void Ring_graphics_menu::save()
{
    static std::string ring_filename_out;
    //jsa_file.resize(128);
    if(ImGui::InputText("filename to write", &ring_filename_out))
    {
        // jsa_file.shrink_to_fit();
        std::cout << "name " << ring_filename_out << " end. Size = " << ring_filename_out.size() <<  std::endl;
    }
    ImGui::SameLine();
    // ImGui::NextColumn();
    if(ImGui::Button("Do it!"))
    {
        std::cout << "yeah!" << std::endl;
        std::string path = "./tempplots/";
        // @TODO FIX FOR WINDOWS 
        path="";
        // write matrix 

        Eigen::MatrixXcd ring_matrix = ring.jsa.matrix;
        write_file(ring_matrix.cwiseAbs(), path + ring_filename_out + ".dat");
    }
}

void Ring_graphics_menu::calc_transmission_powers()
{
    // int num_freq_points = 300;
    int num_freq_points = 3 * ring.get_nspec();
    Eigen::VectorXd freqs = Eigen::VectorXd::LinSpaced(num_freq_points,
            6 * ring.get_start_freq(), 6 * ring.get_end_freq());

    Eigen::VectorXd pump_vals(num_freq_points);
    Eigen::VectorXd ring_power_vals(num_freq_points);

    std::vector<Eigen::VectorXd> vect_ring_power_vals;
    std::vector<Eigen::VectorXd> vect_bus_transfer_vals;
    std::vector<Eigen::VectorXd> vect_ring_bus_vals;

    for(int j = 0; j < ring.get_nrings(); j++)
    {
        Eigen::VectorXd ring_power_vals(num_freq_points);
        Eigen::VectorXd bus_transfer_vals(num_freq_points);
        Eigen::VectorXd ring_bus_vals(num_freq_points);

        for(int i = 0; i < num_freq_points; i++)
        {
            ring_power_vals(i) = abs(ring.ring_power(ring.coupler_mat(freqs(i)))(j,0));
            bus_transfer_vals(i) = norm(ring.bus_transfer(ring.coupler_mat(freqs(i)))(0,j));
            ring_bus_vals(i) = abs( (ring.ring_power(ring.coupler_mat(freqs(i))) * 
                        ring.bus_transfer(ring.coupler_mat(freqs(i))).adjoint())(j,0));
        }
        vect_ring_power_vals.push_back(ring_power_vals);
        vect_bus_transfer_vals.push_back(bus_transfer_vals);
        vect_ring_bus_vals.push_back(ring_bus_vals);
    }

    for(int i = 0; i < num_freq_points; i++)
    {
        pump_vals(i) = ring.pump_func(freqs(i));
    }

    ring_func_plot = Plot(vect_ring_power_vals);
    bus_transfer_plot = Plot(vect_bus_transfer_vals);
    ring_bus_plot = Plot(vect_ring_bus_vals);
    pump_plot = Plot({pump_vals});
}

void Ring_graphics_menu::show_transmission_powers()
{
    // show me the magic
    float plot_width = 500;
    ImGui::Text("Pump over 3 resonances");
    pump_plot.show_inline(plot_width);

    ImGui::Text("Bus transfer functions");
    bus_transfer_plot.show_inline(plot_width);

    ImGui::Text("Ring functions");
    ring_func_plot.show_inline(plot_width);

    ImGui::Text("Ring * Bus.adjoint() functions");
    ring_bus_plot.show_inline(plot_width);
}

void Ring_graphics_menu::show_symplectic()
{
    if(ImGui::TreeNode(" Symplectic matrices "))
    {
        static bool b_source_exists = false;
        static std::vector<opengl_plot> ring_M_alpha_plots;
        static opengl_plot ring_M_beta_plot;

        static Eigen::MatrixXcd ring_M_mat;

        static bool b_remove_vacuum = true;
        ImGui::Checkbox("Remove vacuum contribution from matrix", &b_remove_vacuum);


        if(ImGui::Button("Make source") || (b_auto_update_symp && update_ring))
        {
            Active_unitary ring_M{make_source()};

            std::cout << "ring dim = " << ring_M.matrix.rows() << " cols " 
                << ring_M.matrix.cols() << " compared to " << get_dim() << std::endl;

            std::cout << " ring alpha() dim = " << ring_M.alpha().rows() << std::endl;


            ring_M_alpha_plots.resize(2);

            ring_M_alpha_plots[0].update(ring_M.alpha().cwiseAbs() 
                    - Eigen::MatrixXd::Identity(2.0 * get_dim() , 2.0 * get_dim())
                    , 0, ring_spec);

            ring_M_alpha_plots[1].update(ring_M.alpha().cwiseAbs() 
                    //   - Eigen::MatrixXd::Identity(2.0 * Ring.get_dim() , 2.0 * Ring.get_dim())
                    , 0, ring_spec);

            ring_M_beta_plot.update(ring_M.beta().cwiseAbs(), 0, ring_spec);

            b_source_exists = true;
            ring_M_mat = ring_M.matrix;
        }

        ImGui::SameLine(); 
        static bool b_show_source = true;
        ImGui::Checkbox("Show M symplectic transform", &b_show_source);
        if(b_source_exists && b_show_source)
        {
            ring_M_alpha_plots[!b_remove_vacuum].show(640, 480, false);
            ImGui::SameLine(); 
            ring_M_beta_plot.show(640, 480, false);
        }

        // calc cov 
        static std::vector<opengl_plot> ring_cov_plots_alpha;
        static opengl_plot ring_cov_plots_beta;
        static std::vector<opengl_plot> full_ring_cov_plots;

        static bool b_show_cov = false;
        if(ImGui::Button("Covariance matrix?") || (b_auto_update_symp && update_ring))
        {
            ring_cov_plots_alpha.resize(2);

            std::cout << " hi ring nspace = " << get_nspace() << " spec " << ring_spec << std::endl;
            // State is here 
            State M{Symplectic_matrix{ring_M_mat, 2 * get_nspace(), ring_spec}};

            std::cout << "here1"<<std::endl;
            std::cout << "cov alpha size " << M.cov.alpha().rows() << std::endl;

            ring_cov_plots_alpha[0].update(M.cov.alpha().cwiseAbs() 
                    - Eigen::MatrixXd::Identity(get_dim() * 2, get_dim() * 2)
                    , 0, M.get_nspec());
            ring_cov_plots_alpha[1].update(M.cov.alpha().cwiseAbs() 
                    //        - Eigen::MatrixXd::Identity(Ring.get_dim() * 2, Ring.get_dim() * 2)
                    , 0, M.get_nspec());
            std::cout << "here2" << std::endl;

            ring_cov_plots_beta.update(M.cov.beta().cwiseAbs(), 0, M.get_nspec());

            full_ring_cov_plots.resize(2);

            //  this is causing a segfault for the backscatter case 


            full_ring_cov_plots[0].update(M.cov.matrix.cwiseAbs() - Eigen::MatrixXd::Identity(M.cov.matrix.rows(), M.cov.matrix.cols()) , 0, M.get_nspec());
            full_ring_cov_plots[1].update(M.cov.matrix.cwiseAbs(), 0, M.get_nspec());

            b_show_cov = true;
        }

        if(b_show_cov)
        {
            ImGui::Text("Covariance matrix minus identity");
            ring_cov_plots_alpha[!b_remove_vacuum].show(640, 480, false);
            ImGui::SameLine();
            ring_cov_plots_beta.show(640, 480, false);

            ImGui::Text("Full covariance matrix "); 
            full_ring_cov_plots[!b_remove_vacuum].show(640, 480, false);
        }
        ImGui::TreePop();
    }
}

void Ring_graphics_menu::show_pic()
{
    // show picture 
    //     static float sz = 36.0f;
    //  static float thickness = 3.0f;
    const ImVec2 p = ImGui::GetCursorScreenPos();
    float x = p.x + 4.0f; 
    float y = p.y + 4.0f;

    // Global x, y offset
    x += 200.0f;
    y += 100.0f;

    // Work out position of waveguide
    float x_waveguide{x};
    float y_waveguide{y};
    float waveguide_length{200.0f};

    // Make the waveguide
    Waveguide wave1{x_waveguide, y_waveguide, waveguide_length};
    //wave1.set_center(x_waveguide, y_waveguide);
    //wave1.show();

    std::vector<Draw_Ring> ring_drawings;

    for(int i = 0; i < n_ring; i++)
    {
        if(i == 0)
        {
            ring_drawings.push_back(Draw_Ring{wave1, ring.get_length(0)});
        }
        else
        {
            ring_drawings.push_back(Draw_Ring{ring_drawings[i-1], ring.get_length(0)});
        }
        ring_drawings[i].add_coupling_text(params[i]);
    }
    /*
    // Make ring 1
    Ring ring1(wave1, ring_length1 * 15);
    ring1.add_coupling_text(ring_coupling1);	    

    // Make ring 2
    Ring ring2(ring1, ring_length2 * 15);
    ring2.add_coupling_text(ring_coupling2);	    

    // Make ring 3
    Ring ring3(ring2, ring_length3 * 15);
    ring3.add_coupling_text(ring_coupling3);	    
    */
}
} // end of namespace
