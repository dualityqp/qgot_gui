#include "graphics_wrappers.hpp"

#define STB_IMAGE_IMPLEMENTATION
#include "../libs/stb_image.h"

/// SLIDER DOUBLE 
bool ImGui::SliderDouble(const char* label, double* v, double v_min, double v_max, const char* format, ImGuiSliderFlags flags)
{
    return SliderScalar(label, ImGuiDataType_Double, v, &v_min, &v_max, format, flags);
}

// Simple helper function to load an image into a OpenGL texture with common settings
bool LoadTextureFromFile(const char* filename, GLuint* out_texture, int* out_width, int* out_height)
{
    // Load from file
    int image_width = 0;
    int image_height = 0;
    unsigned char* image_data = stbi_load(filename, &image_width, &image_height, NULL, 4);
    if (image_data == NULL)
        return false;

    // Create a OpenGL texture identifier
    GLuint image_texture;
    glGenTextures(1, &image_texture);
    glBindTexture(GL_TEXTURE_2D, image_texture);
    // glBindTexture(GL_TEXTURE_2D, 0);

    // Setup filtering parameters for display
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    // Upload pixels into texture
    glPixelStorei(GL_UNPACK_ROW_LENGTH, 0);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image_width, image_height, 0, GL_RGBA, GL_UNSIGNED_BYTE, image_data);
    stbi_image_free(image_data);

    *out_texture = image_texture;
    *out_width = image_width;
    *out_height = image_height;

    return true;
}

// 2D plots --------------------------- Plot
Plot::Plot(const std::vector<Eigen::VectorXd> & plots): plot_dob(plots)
{
    // set data stuff 
    num_plots = plots.size();
    buf_size = plots[0].size();
    // double max = 0.0;
    // double min = 0.0;

    for(int i = 0; i < num_plots; i++)
    {
        max = (plot_dob[i].maxCoeff() > max) ? plot_dob[i].maxCoeff() : max;
        min = (plot_dob[i].minCoeff() < min) ? plot_dob[i].minCoeff() : min;
    }
}


void Plot::highlight(double val)
{
    conf.values.x_val_highlight = val;
}

void Plot::xdata(float * xdata)
{
    conf.values.xs = xdata;
}

void Plot::show_inline(float x, float y)
{
    // Draw first plot with multiple sources
    // conf.values.xs = x_data;
    conf.values.count = buf_size;
    // conf.values.ys_list = &plot_data[0];
    conf.values.ys_list = plot_dob;
    // y_data // use ys_list to draw several lines simultaneously
    conf.values.ys_count = num_plots;
    conf.values.colors = colors;
    // conf.scale.min = *std::min_element(plot_dob.begin(), plot_dob.end());
    // conf.scale.max = *std::max_element(plot_dob.begin(), plot_dob.end());
    conf.scale.min = min;
    conf.scale.max = max;

    conf.tooltip.show = true;
    conf.tooltip.format = "x=%.2f, y=%.2f";
    conf.grid_x.show = true;
    conf.grid_x.size = 128;
    conf.grid_x.subticks = 4;
    conf.grid_y.show = true;
    conf.grid_y.size = 0.5f;
    conf.grid_y.subticks = 5;
    conf.selection.show = true;
    conf.selection.start = &selection_start;
    conf.selection.length = &selection_length;

    int width = (x < buf_size) ? buf_size : x;
    int height = (y < 100) ? 100 : y;
    conf.frame_size = ImVec2(width, height);
    ImGui::Plot("plot1", conf);
}

void Plot::show(float x, float y)
{
    ImGui::Begin("Example plot", nullptr, ImGuiWindowFlags_AlwaysAutoResize);
    // ImGui::Begin("Example plot", nullptr);//, ImGuiWindowFlags_AlwaysAutoResize);
    show_inline(x,y);
    ImGui::End();
}
// ----------------------- End of Plot 

// ------------------------------ Opengl contour plots 
opengl_plot::opengl_plot()
{

    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
}

GLuint opengl_plot::rgb_int(std::vector<unsigned int> rgb_vals)
{
    return ((rgb_vals[0] << 24) | (rgb_vals[1] << 16) | (rgb_vals[2] << 8) | (255 << 0));
}

opengl_plot::hsv opengl_plot::rgb2hsv(rgb in)
{
    hsv         out;
    double      min, max, delta;

    min = in.r < in.g ? in.r : in.g;
    min = min  < in.b ? min  : in.b;

    max = in.r > in.g ? in.r : in.g;
    max = max  > in.b ? max  : in.b;

    out.v = max;                                // v
    delta = max - min;
    if (delta < 0.00001)
    {
        out.s = 0;
        out.h = 0; // undefined, maybe nan?
        return out;
    }
    if( max > 0.0 ) { // NOTE: if Max is == 0, this divide would cause a crash
        out.s = (delta / max);                  // s
    } else {
        // if max is 0, then r = g = b = 0              
        // s = 0, h is undefined
        out.s = 0.0;
        out.h = NAN;                            // its now undefined
        return out;
    }
    if( in.r >= max )                           // > is bogus, just keeps compilor happy
        out.h = ( in.g - in.b ) / delta;        // between yellow & magenta
    else
        if( in.g >= max )
            out.h = 2.0 + ( in.b - in.r ) / delta;  // between cyan & yellow
        else
            out.h = 4.0 + ( in.r - in.g ) / delta;  // between magenta & cyan

    out.h *= 60.0;                              // degrees

    if( out.h < 0.0 )
        out.h += 360.0;

    return out;
}

opengl_plot::rgb opengl_plot::hsv2rgb(hsv in)
{
    double      hh, p, q, t, ff;
    long        i;
    rgb         out;

    if(in.s <= 0.0) {       // < is bogus, just shuts up warnings
        out.r = in.v;
        out.g = in.v;
        out.b = in.v;
        return out;
    }
    hh = in.h;
    if(hh >= 360.0) hh = 0.0;
    hh /= 60.0;
    i = (long)hh;
    ff = hh - i;
    p = in.v * (1.0 - in.s);
    q = in.v * (1.0 - (in.s * ff));
    t = in.v * (1.0 - (in.s * (1.0 - ff)));

    switch(i) {
        case 0:
            out.r = in.v;
            out.g = t;
            out.b = p;
            break;
        case 1:
            out.r = q;
            out.g = in.v;
            out.b = p;
            break;
        case 2:
            out.r = p;
            out.g = in.v;
            out.b = t;
            break;

        case 3:
            out.r = p;
            out.g = q;
            out.b = in.v;
            break;
        case 4:
            out.r = t;
            out.g = p;
            out.b = in.v;
            break;
        case 5:
        default:
            out.r = in.v;
            out.g = p;
            out.b = q;
            break;
    }
    return out;     
}

GLuint opengl_plot::rgb_interp_use_hsv(double scale, std::vector<unsigned int> rgb1, std::vector<unsigned int> rgb2)
{
    // make sure is in the range 0  to 1
    if(scale > 1.0) { scale = 1.0; }
    else if(scale < 0.0) { scale = 0.0; } 

    hsv hsv1 = rgb2hsv({rgb1[0]/255.0, rgb1[1]/255.0, rgb1[2]/255.0});
    hsv hsv2 = rgb2hsv({rgb2[0]/255.0, rgb2[1]/255.0, rgb2[2]/255.0});

    hsv outcol;
    double hue_diff = hsv2.h - hsv1.h;
    double delta = hue_diff + ((abs(hue_diff) > 180) ? ((hue_diff < 0) ? 360 : -360) : 0);
    outcol.h = hsv1.h + scale * (delta);
    outcol.s = hsv1.s + scale * (hsv2.s - hsv1.s);
    outcol.v = hsv1.v + scale * (hsv2.v - hsv1.v);

    rgb rgb_out = hsv2rgb(outcol);

    std::vector<unsigned int> out;
    out.push_back(rgb_out.r * 255);
    out.push_back(rgb_out.g * 255);
    out.push_back(rgb_out.b * 255);

    return rgb_int(out);
}

GLuint opengl_plot::rgb_interp(double scale, std::vector<unsigned int> rgb1, std::vector<unsigned int> rgb2)
{
    // make sure is in the range 0  to 1
    if(scale > 1.0) { scale = 1.0; }
    else if(scale < 0.0) { scale = 0.0; } 

    std::vector<unsigned int> out;
    for(int i = 0; i < (int)rgb1.size(); i++) // r g b 
    {
        out.push_back(rgb1[i] + scale * (rgb2[i] - rgb1[i]));
    }
    return rgb_int(out);
}


int opengl_plot::midpoint_search(double val, double bin_size, int max_k, double min_val)
{
    double num = val + min_val;
    int k = 0;
    int min_k = 0;
    int start_max = max_k;
    while(min_k <= max_k)
    {
        k = (max_k + min_k)/2;
        if( (bin_size * k) <= num ) // decrease 
        {
            min_k = k + 1;
            if( num < (bin_size * (k+1))) 
            {
                // found return k 
                if(k == start_max) { return k-1;} // stop overflows
                else { return k; }
            }
        }
        else // increase k 
        {
            max_k = k - 1;
        }
    }
    return 0;
}

void opengl_plot::update(const Eigen::MatrixXd & mat_in, double max_val_in, int block_size)
{
    if(block_size == 0) { block_size = mat_in.rows();}
    // std::cout << " block size " << block_size << " mat in size " << mat_in.size() << std::endl;
    int num_blocks = mat_in.rows() / block_size;

    // Jesus Oli what are you doing.
    int rowpadding = (mat_in.rows() / num_blocks) % 4;
    if (rowpadding == 1) { rowpadding = 3; }
    else if (rowpadding == 2) { rowpadding = 2;}
    else if (rowpadding == 3) { rowpadding = 1;}

    int colpadding = (mat_in.rows() / num_blocks) % 4;
    if (colpadding == 1) { colpadding = 3; }
    else if (colpadding == 2) { colpadding = 2;}
    else if (colpadding == 3) { colpadding = 1;}

    //int im_size = mat_in.rows();
    // N = mat_in.rows() + padding;
    Nrows = mat_in.rows() / block_size;
    Ncols = mat_in.cols() / block_size;
    if(Nrows == Ncols) { N = Nrows;}
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

    // need to sort the data into ranges and assign colours based on ranges 
    int palette_size = 8;
    double max = mat_in.maxCoeff();
    if(max_val_in != 0) { max = max_val_in; }

    double min = mat_in.minCoeff();
    double bin_size = (max - min) / (double)palette_size; 

    // this bad boy causes a segfault when the matrix we a re plotting is too big 
    // GLuint data[mat_in.rows()][mat_in.cols()];
    // pick some max thing to allocate 
    //
    /// @TODO THIS IS PROBABLY CAUSING PROBLEMS 
    // const int max_rows = 5000;
    // const int max_cols = 5000;  
    // GLuint * data = new GLuint[max_rows][max_cols];
    // static GLuint data[max_rows][max_cols];
    std::vector<GLuint> data(mat_in.rows() * mat_in.cols());
    // std::vector<std::vector<GLuint> > data;( mat_in.rows(), std::vector<GLuint> (mat_in.cols(),0));
    // const int data_height = mat_in.rows();
    const int data_width = mat_in.cols();

    // std::cout << "mat size " << mat_in.size() <<  " rows " << mat_in.rows() << " cols " << mat_in.cols() << std::endl;
    // std::cout << "nrow " << Nrows << " ncols " << Ncols << " block size " << block_size << std::endl;
    // openmp coming in clutch
    // make the next 2 for loops run in parallel on $(n_proc) threads
#pragma omp parallel for collapse(4)
    for(int j = 0; j < Ncols; j++)
    {
        // origin top left (0,0)
        for(int i = 0; i < Nrows; i++)
        { 
            for(int k = 0; k < block_size; k++)
            {
                for(int l = 0; l < block_size; l++)
                {
                    // for each point search the sectors to find which bin and color it
                    int pos = midpoint_search(mat_in(i * block_size + l, j * block_size + k), bin_size, (int)spectral_palette.size(), min);
                    double scale = (mat_in(i * block_size + l, j * block_size + k) - (double)pos*bin_size) / (double)(bin_size) ;
                    //origin bot left replace i with mat_in.rows() - 1 - i 
                    int swap_origin = block_size - 1 - l;
                    if(( pos < palette_size - 1) && interpolate_colours)
                    {
                        if(use_hsv_interp)
                        {
                            // data[i * block_size + swap_origin][j * block_size + k] = rgb_interp_use_hsv(scale, spectral_palette[pos], spectral_palette[pos+1]);
                            data[(i * block_size + swap_origin) * data_width + (j * block_size + k)] 
                                = rgb_interp_use_hsv(scale, spectral_palette[pos], spectral_palette[pos+1]); 
                        }
                        else 
                        {
                            // data[i * block_size + swap_origin][j * block_size + k] = rgb_interp(scale, spectral_palette[pos], spectral_palette[pos+1]);
                            data[(i * block_size + swap_origin) * data_width + (j * block_size + k)] = rgb_interp(scale, spectral_palette[pos], spectral_palette[pos+1]);
                        }
                    }
                    else
                    {
                        // data[i * block_size + swap_origin][j * block_size + k] = rgb_int(spectral_palette[pos]);
                        data[(i * block_size + swap_origin) * data_width + (j * block_size + k)] = rgb_int(spectral_palette[pos]);
                    }
                }
            }
        }
    }
    // last padding bits 
    for(int j = mat_in.cols() - colpadding; j < Ncols; j++)
    { 
        for(int i = mat_in.rows() - rowpadding; i < Nrows; i++)
        {
            // why is this here?
            // origin top left [i][j]
            // int swap_origin = mat_in.rows() - 1 - i;
            //// data[swap_origin][j] = rgb_int(spectral_palette[0]);
            //data[(swap_origin) * data_width + (j)] = rgb_int(spectral_palette[0]);
        }
    }

    // this bugger was causing the memory leak.
    ////glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, mat_in.cols(), mat_in.rows(), 0, GL_RGBA, GL_UNSIGNED_INT_8_8_8_8, &data[0]);
    // sub is supposed to be better but i can't get it working 
    // glTexSubImage2D(GL_TEXTURE_2D, 0, GL_RGBA, Ncols, Nrows, 0, GL_RGBA, GL_UNSIGNED_INT_8_8_8_8, data);
    // delete[] data;
}

void opengl_plot::show(int x, int y, bool show_text)
{
    if(show_text)
    {
        ImGui::Text("pointer = %p", (void *)(intptr_t)texture); 
        ImGui::SameLine(); ImGui::Text("size = %d x %d", Ncols, Nrows);
    }
    ImGui::Image((void*)(intptr_t)texture, ImVec2(x, y )); 
}

void opengl_plot::show(bool * window_close)
{
    ImGui::Begin("aOpenGL Texture Text", window_close);
    ImGui::Text("pointer = %p", (void *)(intptr_t)texture); 
    ImGui::SameLine(); ImGui::Text("size = %d x %d", Ncols, Nrows);
    ImGui::Image((void*)(intptr_t)texture, ImVec2(320,240 ));
    ImGui::End();
}

// for plotting block matrices, need to flip spectral blocks so origin is bottom left but
// keep outer blocks (spatial modes) in the correct order origin top left (matrix order)
