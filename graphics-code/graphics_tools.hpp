/**** @file graphics tools class manager windows
 * @author O Thomas
 * @date Dec 2019
 * @brief All windows manager classes are here, main can create however many of each
 * object to show multiple sources or filters etc. The idea is that all of these classes
 * share common setup() and show() methods so the structure main sees is the same
 * regardless of the class object called ( although the paramaters passed are different,
 * the overall structure should be the same
 */

#pragma once
#ifndef graphics_tools_hpp
#define graphics_tools_hpp

#include "graphics_wrappers.hpp"

#include "qgot_public/circuits/optical_circuits.hpp"
#include "qgot_public/circuits/circuits.hpp"

#include "qgot_public/math-code/rings.hpp"

#include <olistd/olistdlib.hpp>
//
#include <functional> // std::bind 
#include <memory> // std::unique_ptr 
#include <future> // std::async, std::future

namespace QGot
{
    // Helper to display a little (?) mark which shows a tooltip when hovered.
    // In your own code you may want to display an actual icon if you are using a merged icon fonts (see docs/FONTS.txt)
    void HelpMarker(const char* desc);

    // bool contains_string(std::vector<std::string> vect, std::string to_find);
    // bool string_found(std::string search, std::string target);
    // int get_int_from_string(std::string str);

    void histogram_from_Nfolds();

    class Histograms_from_Nfolds 
    {
        private:
        public:

            Meas::Nfolds nfolds;
            std::string label;
            float histogram_width;
            bool update_histogram = true;

            bool b_show_all_vals = true;
            double CHOP_DOUBLE = 1e-10;
            float max_val = 0.0;

            int n_folds = 0;
            // holds the n- click nfolds e.g. first element is all the singles. second is all
            // the two-folds etc.
            std::vector<float> histogram_data;
            std::vector<std::string> histogram_labels;
            std::vector<std::string> histogram_labels2;

            Histograms_from_Nfolds(const Meas::Nfolds & nfolds_in, std::string label_in, float histogram_width_in) : nfolds(nfolds_in), label(label_in), histogram_width(histogram_width_in) 
        {
        }

            void reset()
            {
                histogram_data.resize(0);
                histogram_labels.resize(0);
                histogram_labels2.resize(0);
            }

            void update(const Meas::Nfolds & nfolds_in, std::string label_in, float histogram_width_in)
            {
                nfolds = nfolds_in;
                label = label_in;
                histogram_width = histogram_width_in;

                reset();

                ImGui::InputInt("%s - folds", &n_folds); 
                // wrap round
                int max = nfolds.nfolds.size() - 1;
                if(n_folds < 0) 
                    n_folds = max;
                if(n_folds > max) 
                    n_folds = 0;
                // all nfolds of size n 
                std::vector<Meas::Nfold> results = nfolds.nfolds[n_folds];

                // nfolds.print();
                // turns Meas::Nfolds into vector of floats and vector of strings for labels 
                // Meas::Nfolds res = M.bucket_detector(detector_modes); 
                for(const Meas::Nfold & res : results)
                {
                    auto click_pattern = res.get_click_pattern();
                    for(auto it = click_pattern.begin(); it != click_pattern.end(); it++)
                    {
                        if(b_show_all_vals || abs(it->second) >= CHOP_DOUBLE)
                        {
                            histogram_data.push_back((float)it->second);
                            histogram_labels.push_back(res.get_modes_str()); // modes 
                            histogram_labels2.push_back(olistd::vect_to_string(it->first)); // click pattern 
                        }
                    } // otherwise hide small values 
                }

                double val = *std::max_element(histogram_data.begin(), histogram_data.end());
                max_val = max_val < val ? val : max_val;

                update_histogram = false;

                ImGui::PlotHistogram(("##"+label).c_str(), &histogram_data[0], histogram_data.size(), histogram_data.size(), label.c_str(), 0.0f, max_val, ImVec2(histogram_width,150));

                // mode labels 
                float spacing = (float)histogram_width / (float)(histogram_data.size()+0);
                ImGui::Text(" ");
                for(int i = 0; i < (int)histogram_labels.size(); i++)
                {
                    ImGui::SameLine((i+1.0)*spacing); ImGui::Text("%s", histogram_labels[i].c_str());
                }
                // click pattern labels 
                ImGui::Text(" ");
                for(int i = 0; i < (int)histogram_labels.size(); i++)
                {
                    ImGui::SameLine((i+1.0)*spacing); ImGui::Text("%s", histogram_labels2[i].c_str());
                }

                if(ImGui::Button("fix scaling"))
                {
                    max_val = *std::max_element(histogram_data.begin(), histogram_data.end());
                    update_histogram = true;
                }

                ImGui::SameLine();
                if(ImGui::Button("hide small vals"))
                {
                    b_show_all_vals = !b_show_all_vals;
                    update_histogram = true;
                }
            } // end of histogram plotting 
    };


    /// ------------------------------------------------ Source options --------------------
    /// @brief Manages all sources objects, currently rings are not included here 
    /// called from main 
    class Source_graphics_menu
    {
        private:
            // params 
            std::string func_choice = "Sinc";
            double xoffset = 0;
            double yoffset = 0;

            double phasematching = 0.0;
            double bandwidth = 1.0; 

            // const int num_params = 6; // e.g size, power, xoff, yoff, func_choice

            bool update_jsa = true; // otherwise blank until something changes 
            bool changed_flag = true;
            bool new_window = false;

            int jsa_size = 20;
            double squeezing_power = 0.1;
            // liist box
            struct listbox_item {
                std::string name;
                bool selected = false;

                listbox_item(std::string option_name, bool flag)
                {
                    name = option_name;
                    selected = flag;
                }
                listbox_item(std::string option_name)
                {
                    name = option_name;
                }
                void add_option(std::string option)
                {
                    name = option;
                    selected = false;
                }
            };

            /// set the choice of JSA functions here. 
            std::vector<listbox_item> jsa_funcs_list = {listbox_item("Gaussian", false), 
                listbox_item("Sinc", true), listbox_item("Ring", false)};

            int schmidt_mode_counter = 0;
            std::string time_taken ="";

        public:
            // these 2 should probs be private :w
            Jsa jsa;
            opengl_plot plot;

            std::string jsa_location;
            bool get_from_file = false;

            // ---------------------------- Methods -----------------------------------
            Source_graphics_menu();

            ~Source_graphics_menu();

            void reset();

            void show(int spec_size);

            Jsa make_jsa(int spec_size) const; 
            Jsa make_jsa(int spec_size, double sq_power) const; 

            Jsa make_jsa_fromfile(std::string filename);

            bool changed();
    };

    /// -------------------------------------- Filter options --------------------------------
    /// Spectral filter menu options 
    /// @TODO This and the Source menu should inherit from a larger class as many of the
    /// options are the same between the two
    class Filter_graphics_menu
    {
        private:
            // params 
            std::string func_choice = "bandpass";
            int filter_width = 20;
            double xoffset = 0;
            double yoffset = 0;

            // const int num_params = 6; // e.g size, power, xoff, yoff, func_choice

            bool update_filter = true; // otherwise blank until something changes 
            bool changed_flag = true;
            bool new_window = false;

            int jsa_size;

            // liist box
            struct listbox_item {
                std::string name;
                bool selected = false;

                listbox_item(std::string option_name, bool flag)
                {
                    name = option_name;
                    selected = flag;
                }
                listbox_item(std::string option_name)
                {
                    name = option_name;
                }
                void add_option(std::string option)
                {
                    name = option;
                    selected = false;
                }
            };

            // get this from exisiting sources and preview what they would look like under
            // filter 
            Jsa jsa;
            opengl_plot plot;

            // functional shape choices 
            std::vector<listbox_item> filter_funcs_list = {listbox_item("bandpass", true), 
                listbox_item("Gaussian", false)};

            Filter filter;
        public:

            Filter_graphics_menu();

            ~Filter_graphics_menu();

            void reset();

            void show(int spec_size);

            Filter make_filter(int spec_size) const;

            bool changed();
    };


    // functions not in a class 
    //
    // apply operators 
    void circuit_to_apply(const Circuit_op & op, State & M, std::vector<int> & detector_modes, const std::vector<Source_graphics_menu> & sources, const std::vector<Filter_graphics_menu> & filters, const double power, const int jsa_size);

    /// ------------------------------ optical circuit builder  --------------------
    /// oh dear optical circuit class that the circuit manager calls and manages 
    class Circuit
    {
        private:

            int num_modes;
            int num_components;
            int old_modes;
            int old_components;

            int circuit_size;

            // gate list, never changes 
            int gate_size;

            std::vector<Circuit_op> gates = { Gate::Source, Gate::Singlesource, Gate::Beamsplitter, Gate::Filter, Gate::Phase, Gate::Loss, Gate::Thermal, Gate::Click_Detector, Gate::Vacuum_Detector, Gate::PNR_Detector, Gate::Empty};


            int num_sources;
        public:
            // where the magic happens 
            // std::vector<std::string> circuit;
            std::vector<Circuit_op> circuit;

            Circuit();

            void save();

            void load();

            void set_num_modes(int num_modes_in);

            void drag_drop();
            void show(int sources_count);

            // circuit[pos].substr(0,6) == "Source"
            std::string incr_source(std::string item);

            std::string deincr_source(std::string item);

            // takes abs pos in vector and returns row coord
            int get_row(int pos) { return pos / num_components; }
            // takes abs pos in vector and returns col coord
            int get_col(int pos) { return pos % num_components; }

            /// takes row, col and returns abs pos n
            int get_pos(int row, int col) { return row * num_components + col; }

            int get_num_modes() { return num_modes; }

            struct string_ints {
                std::string name = "";
                std::vector<int> modes;

                string_ints() 
                {
                    name = "";
                    modes = {-1};
                }
                string_ints(std::string name_in, std::vector<int> mode_in) : name(name_in), modes(mode_in)
                {}
            };

            /// ----------------------------------- circuit output 
            /// takes the circuit str and returns gates with modes they act on 
            /// crucially in the correct order!
            std::vector<string_ints> circuit_output();
    };

    /// ----------------------- Optical circuits, states and measurement results 
    class Circuit_graphics_menu {
        private: 
            std::string time_taken = "0.0";
            bool data_exists = false;

            // detectors 
            // Threshold detectors 
            std::vector<float> bucket_histogram_data;
            std::vector<std::string> bucket_hist_labels;

            // Number resolving
            std::vector<float> fock_histogram_data;
            std::vector<std::string> fock_hist_labels;

            // correlation function measurements 
            std::vector<float> correlation_histogram_data;
            std::vector<std::string> correlation_hist_labels;

            // plots for covariance matrix elements 
            std::vector<opengl_plot> cov_plots;

            int space_dim = 4;
            int spec_dim; // = jsa_size;
            Symplectic_matrix symp_transform;

            int photon_cutoff = 1; // for number resolving 
            // bool b_remove_vacuum = true;

        public:
            State M; // M(space_dim, spec_dim)
        private:
            bool load_file1 = false;
            bool b_update_cov_plot = false;

            // covariance matrix plottitng
            bool normal_basis = true;
            bool mode_basis = false;

            std::vector<opengl_plot> g2_plots;

            double g1s_max_val = 0.0;
            std::vector<std::vector<float> > g1s_plots_xdata; 
            std::vector<std::vector<float> > g1s_plots_ydata; 

            opengl_plot plot;

            double willsscale = 1.0;

            int cov_width = 640; // default 
            int cov_height = 480; // default 

            std::vector<Eigen::MatrixXd> cov_elements_data;

            std::vector<Eigen::MatrixXd> g2s;

            bool update_bucket_histogram = true;
            bool update_fock_histogram = true;
            bool update_correlation_histogram = true;

            // double bucket_max_val = 0.0;
            // double fock_max_val = 0.0;
            // double correlation_max_val = 0.0;

            // int histogram_width = 800;

            std::vector<Eigen::VectorXd> g1s;

            int num_sources = 0;

            bool test = false;

            // cov matrix plot 
            opengl_plot cov_plot;
            int cov_plot_width = 640;
            int cov_plot_height = 480;

            // stimulated emission 
            // int SET_mode = 0;

            std::string filename_out;

            // bool b_show_all_bucket_vals = true;
            // bool b_show_all_fock_vals = true;
            // bool b_show_all_correlation_vals = true;
            std::vector<int> detector_modes;
            // to stop the simulation step blocking 
            // std::unique_ptr<std::future<void> > s1;
            std::shared_ptr<std::future<void> > s1;
            //    std::vector<std::future<void>> s1s;



            // for a variable operator 
            std::vector<double> op_vals;
            std::vector<Meas::Nfolds > bucket_vals;
            std::vector<Meas::Nfolds > fock_vals;
            std::vector<Meas::Nfolds > correlation_vals;

            std::vector<Covariance_matrix> covariance_matrices_variables;

            bool variable_data_exists = false;


        public:
            // the manager has a circuit object (defined above)
            Circuit circuit;

            // default constructor 
            Circuit_graphics_menu();

            void load_covariance_matrix(int & jsa_size);
            void set_up(int & jsa_size);

            /// returns detector modes 
            std::vector<int> sim(int jsa_size, const std::vector<Source_graphics_menu> & sources, const std::vector<Filter_graphics_menu> & filters, bool run_flag = false, double power = -1);

            // returns false normally, if circuit is simulated return true to let main know
            // the results may have changed and it can be redrawnn
            bool run(int jsa_size, const std::vector<Source_graphics_menu> & sources, const std::vector<Filter_graphics_menu> & filters, bool run_flag = false, double power = -1);

            void show_circuit();

            bool calc_data(const std::vector<int> & detector_modes);
            bool calc_number_resolved(const std::vector<int> & detector_modes);

            void show_covariance_plots();

            void show_bucket_plots()
            {
                show_bucket_plots(M.bucket_detector_marginals(detector_modes));
            }
            void show_fock_plots()
            {
                show_fock_plots(M.pnr_detector(detector_modes, photon_cutoff));
            }
            void show_correlation_plots()
            {
                show_correlation_plots(M.correlation_functions(detector_modes));
            }

            void show_bucket_plots(const Meas::Nfolds & res);
            void show_correlation_plots(const Meas::Nfolds & res);
            void show_fock_plots(const Meas::Nfolds & fock_res);

            void set_all_modes_detectors()
            {
                std::vector<int> modes(M.get_nspace());
                std::iota(modes.begin(), modes.end(), 0);
                detector_modes = modes;
            }
            // void show_circuit();
            // plots specific probabilities as function of varying parameter
            void show_variable_plots();

            // displays all plots
            void show_plots();

            void set_test(bool flag) { test = flag;}// std::cout << "testing " << flag << std::endl;}

    // after calculating detection probabilitites plot the covaraince matrix -
    // Identity 
    void update_cov_plot();
    void update_cov_plot(const Covariance_matrix & cov);
    // display thew currently calculated covariance matrix 
    void show_cov();

    void save_output();

    // fix measurement basis that is persistent between covariance matrix simulations 
    Measurement_unitary meas_unitary;
    bool change_basis = false;
    void set_random_basis()
    {
        set_all_modes_detectors();
        meas_unitary = Measurement_unitary{detector_modes, 1};
        change_basis = true;
    }
    void clear_meas_basis()
    {
        change_basis = false;
    }

    void check_commute()
    {
        // if(ImGui::Button("Check commuting"))
        {
            for(int i = 0; i < space_dim; i++)
            {
                for(int j = 0; j < space_dim; j++)
                {

                    M.cov.modes_commute({i,j});
                }
            }
        }
        //ImGui::PushItemWidth(-1);
    }
    // set up 
    void stimulated_emission_tomography()
    {
        // M.disp.stimulate_all_freqs(SET_mode);
        // std::cout << M.disp.vector << std::endl;
    }


};


class Waveguide {

    float center_x;
    float center_y;
    float length;
    ImU32 col;

    public:

    Waveguide(float x, float y, float length)
        : center_x(x), center_y(y), length(length) {

            // Set color.
            col = IM_COL32(0,255,255,255);

            // Draw the shape
            show();

        }

    float get_x() {
        return center_x;
    }

    float get_y_top() {
        return center_y;
    }

    float get_length() {
        return length;
    }

    void set_center(float x, float y) {
        center_x = x;
        center_y = y;
    }

    void show() {

        // Not sure whether it is a good idea to have this here
        ImDrawList * draw_list = ImGui::GetWindowDrawList();

        // Dimensions
        float thickness = 3.0f;
        ImVec2 p1{center_x - length/2.0f, center_y};
        ImVec2 p2{center_x + length/2.0f, center_y};

        // Draw the waveguide
        draw_list -> AddLine(p1, p2, col, thickness);

    }

};

class Draw_Ring {

    double length;
    float width; /// Actually the height
    float vertical_offset;
    ImU32 col;
    ImU32 col_coupling;
    float center_x;
    float center_y;

    double max_coupling_length;

    public:

    /**
     * \brief Construct a ring on a waveguide
     *
     * Use this constructor to add a ring on top of a 
     * waveguide. The ring will ge placed a fixed
     * distance above the waveguide. You can set the
     * coupling later. There's no need to pass any
     * coordinates. Pass an initial length for the ring
     * The height (width) gets set automatically.
     *
     */
    template<typename T>
        Draw_Ring(T coupled_to, double length) : length(length), width(50.0f), vertical_offset{6.0f}
    {
        // Set position;
        center_x = coupled_to.get_x();
        center_y = coupled_to.get_y_top() - width/2.0f - vertical_offset;

        // Set color.
        col = IM_COL32(0,255,255,200);
        col_coupling = IM_COL32(255,255,0,255);

        // Set max coupling length
        max_coupling_length = coupled_to.get_length();

        // Draw the shape
        show();
    }

    double get_length() { 	return length; }

    void add_coupling_text(double coupling) 
    {

        // Create coupling colour
        col_coupling = IM_COL32(255,255,0, 100 + coupling * 155);

        // Create the coupling region
        float coupling_length = std::min(max_coupling_length, length) * (1 + coupling/10); 
        ImVec2 p_min{center_x - coupling_length/2.0f, center_y + width/2.0f + vertical_offset - 1.0f};
        ImVec2 p_max{center_x + coupling_length/2.0f, center_y + width/2.0f + 1.0f};

        // Solid line in the coupling region between these two points
        ImVec2 p1{center_x - coupling_length/2.0f, center_y + width/2.0f + vertical_offset/2.0f};
        ImVec2 p2{center_x + coupling_length/2.0f, center_y + width/2.0f + vertical_offset/2.0f};

        // Set up coordinates
        float y_point{center_y + width/2.0f + vertical_offset/2.0f};

        // The amount to offset the text horizontally from center
        float h_offset{(float)length/2.0f + 15.0f};

        // Not sure whether it is a good idea to have this here
        ImDrawList * draw_list = ImGui::GetWindowDrawList();

        // Dimensions
        float thickness{1.0f};
        ImVec2 text_pos{center_x + h_offset + 79.0f, center_y - 3.0f};

        // I couldn't make this work with a standard vector,
        // but maybe I was doing something stupid. It's as
        // if there's an ImGUI bug. (The text wasn't showing
        // if you use std::vector)
        ImVec2 points[5];
        points[0] = ImVec2{center_x + coupling_length/2.0f, y_point};
        points[1] = ImVec2{center_x + h_offset + 5.0f, y_point};
        points[2] = ImVec2{center_x + h_offset + 55.0f, center_y};
        points[3] = ImVec2{center_x + h_offset + 75.0f, center_y}; 
        points[4] = ImVec2{center_x + h_offset + 75.0f, center_y + 10.0f}; 

        std::string text = std::to_string(coupling);
        draw_list -> AddText(text_pos, col, text.c_str());
        draw_list -> AddPolyline(points, 5, col, false, thickness);

        // Draw a solid line in the couping region
        draw_list -> AddLine(p1, p2, IM_COL32(255,255,0,255));

        // Draw coupling region
        draw_list -> AddRectFilled(p_min, p_max, col_coupling);

    }


    void set_shape(double length_in, double width_in)
    {
        length = length_in;
        width = width_in;
    }

    double get_x() { return center_x; }

    double get_y_top() { return center_y - width/2.0f; }

    void show()
    {
        // Not sure whether it is a good idea to have this here
        ImDrawList * draw_list = ImGui::GetWindowDrawList();

        // Dimensions
        double thickness = 3.0f;
        ImVec2 center1{center_x - (float)length/2.0f, center_y};
        ImVec2 center2{center_x + (float)length/2.0f, center_y};
        double radius{width/2.0f};

        draw_list -> PathArcTo(center1, radius, M_PI/2.0f, 3*M_PI/2.0, 100);
        draw_list -> PathLineTo(ImVec2(center_x + length/2.0f, center_y - radius));
        draw_list -> PathArcTo(center2, radius, -M_PI/2.0f, M_PI/2.0, 100);
        bool closed{true};
        draw_list -> PathStroke(col, closed, thickness);
    }


};


//// --------------------------------- RING MANAGER CLASS -------------------------------
/// this is a bit messy at the moment 
// wills code 
// top part //
//
// args 
// mnum = 2 // num ring modes
// bnum = 3 // bus modes 
//
// mat = U_Ring   U_RB 
//       U_BR     U_Bus
//
// bus_transfer(c11, c12, c21, c22) = c22 + c21 * (Ident - c11)^-1 * c12
// // @TODO is this right?
// ring_power(c11, c12, c21, c22) = c12 + c11 * (Ident - c11)^-1 * c12
//
// coupler_mat(eta) = [ eta,   sqrt(1-eta^2) ]
//                    [ -sqrt(1-eta^2),  eta ]
//
// Transmission_mat(x) = ident with element = x
//
// coupler_full(k1, k12, ka1, ka2, k) = coupler(k1v, {1,2}) 
// * coupler_mat(ka1, {1,3})
// * transformation(exp(i k vgroup lengths[1] / 2))  // pos 1
// * coupler_mat(k12, {0,1})
// * transformation(exp(i k vgroup lengths[2])) // pos 0 missing 1/2????
// * coupler_mat(ka2, {0,4})
// * transformation(exp(i k vgroup lengths[1]/2)) // pos 1
//
// nbustransfer(k1, k12, ka1, ka2, block)
// { 
//  A = coupler_full(k1,k12,ka1,ka2).block
//  return bus_transfer(A)
//  }
//
// nringpower(k1, k12, ka1, ka2, block)
// {
// A = coupler_full(k1, k12, ka1, ka2)
// return ring_power(A)
// }
//
// idcheck(k1, k12, ka1, ka2) = nbustransfer(...) *nbustransfer(...).adjoint()
//
// pumpfunc(sigma, k) = exp(- (k/sigma)^2) 
//
// res_sep = Pi / ( vgroup * lengths[2])
// pump_bandwidth = Pi / ( vgroup * lengths[2] )
//
// pumpconvolve(params) 
// {
// dw 
// -res_sep / 2 to res_sep/2 in steps of res_sep/ (res - 1)) ?? 
//  kp_start = - res_sep * 0.5
//  dk = res_sep / (res - 1) 
//  for(int j = 0; j < num_freqs; j++)
//  {
//  for(int i = 0; i < mnum; i++)
//  {
//  kp = kp_start + j * dk;
//  mat(i,j) = pumpfunc(pump_bandwidth, kp) * nringpower(params, block =  kp)[i,1]
//  }
//  }
//  // should return a n_spec wide by mnum tall matrix  
//
// resolution
// jsabyconvolutions()
// pump matrix = pumpfuncconvolve
// pumpconvolve(pump_mat, pump_mat)
// pumpconvolveds = res/resfactor
// pumpcon

class Ring_graphics_menu 
{
    private: 

        // modes, bus, spec, kparams 
        int ring_spec = 101;
        int n_ring = 2;
        int n_bus = 3;

        int ring_sys_picker = 0;
        /// config 1 2 rings 3 bus modes (Massimo)
        double k1 = 0.8;
        double k2 = 0.97;
        double ka1 = 0.97;
        double ka2 = 0.97;
        std::vector<double> std_params = {k1, k2, ka1, ka2};

        /// config 2 2 rings 4 bus modes (Backscatter)
        double k1_back = std::sqrt(0.85);
        double kl_back = std::sqrt(0.9);
        double kbc_back = std::sqrt(0.99);
        double kbs_back = std::sqrt(0.97);
        double kphi_back = 0.2;
        std::vector<double> backscatter_params = {k1_back, kl_back, kbc_back, kbs_back, kphi_back};
        // set to either std or backscatter in set_up
        std::vector<double> params;

        // Plots 
        int ring_width = 840;
        int ring_height = 540;
        opengl_plot ring_plot;            
        Plot ring_func_plot;
        Plot bus_transfer_plot;
        Plot ring_bus_plot;
        Plot pump_plot;

        // Flags 
        bool update_ring = true;
        bool b_auto_update_symp = false;

        // generic stuff 
        std::string time_taken = "0.0";

        // private set_up calls this 
        void set_initial_vals();
        void calc_transmission_powers();

    public:
        int get_dim(void) const { return n_bus * ring_spec; }
        int get_nspace(void) const { return n_bus; }
        int get_nspec(void) const { return ring_spec; }

        // ring object  
        Ring_resonator ring;

        /// -------------------------- methods ----------------------------------
        Ring_graphics_menu() 
        {
            set_initial_vals();
            ring = Ring_resonator{n_ring, n_bus, ring_spec, params};
        }

        void set_up();

        // calcs happen here 
        void run();

        void show();

        void save();

        void show_transmission_powers();

        // gets the jsa and builds an active M as a source 
        Active_unitary make_source()
        {
            // ring.matrix is the jsas
            std::cout << "Made active unitary from ring" << std::endl;
            return Active_unitary(Jsa{ring.matrix});
        }

        void show_pic();
        // displays the symplectic transformation and covariance matrix 
        void show_symplectic(void);
}; 

} // end of namespace
#endif
