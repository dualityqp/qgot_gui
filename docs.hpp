/***
 * @file Documentation used in program
 * @ stuff
 */

#include <string>
#include <vector>

#pragma once
#ifndef DOCS_HPP
#define DOCS_HPP


namespace docs
{

    extern std::vector<std::vector<std::string> > ALL_DOCS;

    enum class doc_key
    { 
        sources, 
        filters, 
        passive, 
        active, 
        circuit, 
        measurements
    };

class Docs 
{
    private:
    public:

        Docs() {};
        ~Docs() {};

};
} // end of docs namespace


#endif // DOCS_HPP
