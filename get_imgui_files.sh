dir=./libs/imgui 
exampledir=$dir/examples
cp $dir/imgui.cpp .
cp $dir/imgui.h .
cp $dir/imgui_widgets.cpp .
cp $dir/imgui_demo.cpp .
cp $dir/imgui_draw.cpp .
cp $dir/imconfig.h .

cp $exampledir/imgui_impl_glfw.cpp .
cp $exampledir/imgui_impl_glfw.h .
cp $exampledir/imgui_impl_opengl3.cpp .
cp $exampledir/imgui_impl_opengl3.h .

cp $exampledir/example_glfw_opengl3/main.cpp gui_test_default.cpp
cp $exampledir/libs/gl3w/GL/gl3w.c .
cp $exampledir/libs/gl3w/GL/gl3w.h .
